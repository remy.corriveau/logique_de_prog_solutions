# Nicolas est devenu un fou des chats, il pense que ça le rend charmant. 
# Il a 100 chats ! Une journée où son isolation de COVID le rend un peu cinglé, 
# il décide de faire un grand rond avec ses chats et de leur mettre des chapeaux. 
# Cependant, il a une méthode bien particulière. 
# À chaque fois qu’il passe devant un chat sans chapeau il lui en met un, mais s’il a déjà un chapeau, il lui enlève. 
# Nicolas va faire 100 tours, voici comment il procède.

# Le premier tour, il arrête à chaque chat, ils auront donc tous un chapeau.
# Le deuxième tour, il arrête seulement à chaque 2 chats.
# Le troisième tour, il arrête à chaque 3 chats, et ainsi de suite.
# Le centième et dernier tour, il va arrêter seulement au 100e et dernier chat.

# Finalement, votre application doit afficher quels chats ont encore un chapeau sur la tête et dites à Nicolas qu’il a besoin de nouveaux passe-temps.

# Création de la liste de 100 chats
ls_chats = []
for i in range(100):
    ls_chats.append(False)

# Boucle des 100 tours
for tour in range(100):
    # Bouche des 100 chats V2
    for ind_chat in range(tour, 100, tour + 1):
        if ls_chats[ind_chat]:
            ls_chats[ind_chat] = False
        else:
            ls_chats[ind_chat] = True

# Boucle d'affichage V2
# Enumerate permet de sortir l'index et la valeur d'une liste.
for index, chat in enumerate(ls_chats):
    if chat:
        print(f"Le {index + 1:3}e chat a un chapeau.")
