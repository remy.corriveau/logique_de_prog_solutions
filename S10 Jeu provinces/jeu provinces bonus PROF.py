# Importation des modules
import random
import sys
import textwrap


def trouver_bonne_reponse(dt_provinces: dict) -> tuple[str, str]:
    """
    Retourne une province et sa capitale aléatoire.
    :param dt_provinces: Le dict des provinces et capitales.
    :return: Une province et sa capitale.
    """
    return random.choice(list(dt_provinces.items()))


def creer_choix_reponses(capitale: str, dt_provinces: dict) -> list:
    """
    Retourne quatre capitales différentes, incluant la bonne réponse, pour les choix de réponses.
    :param capitale: La bonne réponse.
    :param dt_provinces: Le dict des provinces et capitales.
    :return: Une liste de quatre capitales différentes.
    """
    # Jusqu'à ce que ce soit 3 capitales différentes de la bonne réponse.
    choix_reponse = [capitale]
    while capitale in choix_reponse:
        choix_reponse = random.sample(list(dt_provinces.values()), k=3)

    # Ajoute la bonne réponse dans les choix et mélange les choix
    choix_reponse.append(capitale)
    random.shuffle(choix_reponse)
    return choix_reponse


def afficher_question(province: str, choix_reponses: list) -> str:
    """
    Affiche la question et les choix de réponses.
    :param province: Le dict des provinces et capitales.
    :param choix_reponses: Les choix possibles.
    :return: La réponse de l'utilisateur, chaine vide s'il veut quitter.
    """
    while True:
        # LIRE réponse (capitale de province).
        reponse = input(textwrap.dedent(f"""
        Quel est la capitale de {province} ? 
        1) {choix_reponses[0]}
        2) {choix_reponses[1]}
        3) {choix_reponses[2]}
        4) {choix_reponses[3]}

                Entrez votre réponse : """))

        # Validation de la réponse
        if reponse not in ("1", "2", "3", "4", ""):
            print("Le choix doit être 1, 2, 3 ou 4. Réessayez.")
        else:
            return reponse


def affichage_resultat(pointage: int, tentative: int) -> None:
    """
    Calcule le score, affiche le résultat et termine l'application.
    :param pointage: Les points obtenus.
    :param tentative: Les tentatives effectuées.
    :return: Rien.
    """
    # Calculer le score
    pourcent_resultat = pointage / tentative * 100

    # Affichage final
    print(f"\nTu as eu {pourcent_resultat:.0f} %")
    if pourcent_resultat < 50:
        print("Travail plus fort.")
    elif pourcent_resultat < 60:
        print("Ça s’en vient !")
    elif pourcent_resultat < 80:
        print("C’est bon, continu.")
    else:
        print("Super, tu es prêt !")

    sys.exit()


def verifier_bonne_reponse(reponse: str, choix_reponses: list, capitale: str) -> int:
    """
    Vérifie si la réponse est bonne et retourne le nombre de points (0, 1).
    :param reponse: La réponse de l'utilisateur.
    :param choix_reponses: La liste des choix de réponses.
    :param capitale: La bonne réponse.
    :return: 1 si la réponse est bonne, 0 sinon.
    """
    # Convertir la réponse en integer pour la position
    index_reponse = int(reponse) - 1

    # Vérifier que la réponse est bonne.
    if choix_reponses[index_reponse].lower() == capitale.lower():
        print("C'est exact!")
        return 1  # Pointage augmente.
    else:
        print(f"Nope! La bonne réponse est {capitale}")
        return 0  # Pointage n'augmente pas.


def jeu_provinces() -> None:
    """
    Demande à l'utilisateur de nommer la capitale d'une province aléatoire.
    Lorsque l'utilisateur entre une réponse vide, le jeu se termine et affiche le score.
    :return: Rien.
    """

    # Les provinces associées à leur capitale.
    dt_provinces = {
        "Yukon"                : "Whitehorse",
        "TNO"                  : "Yellowknife",
        "Nunavut"              : "Iqaluit",
        "Terre-Neuve"          : "St-John's",
        "IPE"                  : "Charlottetown",
        "Nouvelle-Écosse"      : "Halifax",
        "Nouveau-Brunswick"    : "Fredericton",
        "Québec"               : "Québec",
        "Ontario"              : "Toronto",
        "Manitoba"             : "Winnipeg",
        "Saskatchewan"         : "Regina",
        "Alberta"              : "Edmonton",
        "Colombie-Britannique" : "Victoria"
    }
    pointage = 0
    tentative = 0

    # Message de bienvenu
    print("""Bonjour et bienvenu au jeu des provinces !
    Pour sortir, laisser la réponse vide.""")

    while True:  # Se termine si une réponse vide est entrée (affichage_resultat()).
        # Retourne une province aléatoire. (BONNE RÉPONSE)
        province, capitale = trouver_bonne_reponse(dt_provinces)

        # Retourne trois capitales (différentes) pour les choix de réponses. (MAUVAISES RÉPONSES)
        choix_reponses = creer_choix_reponses(capitale, dt_provinces)

        # LIRE réponse (capitale de province).
        reponse = afficher_question(province, choix_reponses)

        # Si l'input est vide, on va afficher le score et fermer l'application.
        if reponse == "":
            affichage_resultat(pointage, tentative)
        else:
            # Compteur de tentatives
            tentative += 1
            # Augmenter (ou pas) le score.
            pointage += verifier_bonne_reponse(reponse, choix_reponses, capitale)


if __name__ == '__main__':
    jeu_provinces()
