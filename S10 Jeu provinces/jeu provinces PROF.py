# Le jeu doit afficher une province ou une capitale au hasard et 
# demander à l’utilisateur d’écrire le nom de la capitale de celle-ci. 
# Si l’utilisateur n’entre aucune réponse, le jeu termine et affiche le pointage. 

# Selon le pointage, un message d’encouragement sera affiché. 
# [0-50%] : Travail plus fort.
# [50-60%] : Ça s’en vient ! 
# [60-80%] : C’est bon, continu. 
# [80-100%] : Super, tu es prêt !

# Important : 
# Il ne faut pas tenir compte de la casse pour la réponse.

# Importation des modules
import random

# Les provinces associées à leur capitale.
dt_provinces = {
    "Yukon"                : "Whitehorse",
    "TNO"                  : "Yellowknife",
    "Nunavut"              : "Iqaluit",
    "Terre-Neuve"          : "St-John's",
    "IPE"                  : "Charlottetown",
    "Nouvelle-Écosse"      : "Halifax",
    "Nouveau-Brunswick"    : "Fredericton",
    "Québec"               : "Québec",
    "Ontario"              : "Toronto",
    "Manitoba"             : "Winnipeg",
    "Saskatchewan"         : "Regina",
    "Alberta"              : "Edmonton",
    "Colombie-Britannique" : "Victoria"
}
pointage = 0
tentative = 0

# Message de bienvenu
print("Bonjour et bienvenu au jeu des des provinces!")

while True:
    # Retourne une province aléatoire.
    province, capitale = random.choice(list(dt_provinces.items()))

    # LIRE réponse (capitale de province).
    reponse = input(f"\nQuel est la capitale de {province} ? ")

    # Sortir si l'input est vide.
    if reponse == "":
        break

    # Vérifier que la réponse est bonne.
    if reponse.lower() == capitale.lower():
        print("C'est exact!")
        # Garder le score.
        pointage += 1
    else:
        print(f"Nope! La bonne réponse est {capitale}")

    # Compteur de tentatives
    tentative += 1

# Calculer le score
pourcent_resultat = pointage / tentative * 100

# Affichage final
print(f"\nTu as eu {pourcent_resultat:.0f} %")
if pourcent_resultat < 50:
    print("Travail plus fort.")
elif pourcent_resultat < 60:
    print("Ça s’en vient !")
elif pourcent_resultat < 80:
    print("C’est bon, continu.")
else:
    print("Super, tu es prêt !")