import pytest
import fn_inventaire
import json


def test_lire_inventaire():
    with open("inventaire.json", "r", encoding="utf-8") as f:
        dt_inventaire = json.load(f)
    assert fn_inventaire.lire_inventaire() == dt_inventaire

@pytest.mark.parametrize("input_clavier, no_inventaire, dict_voiture",
[
    ([666, "Chevrolet", "Impala", "Noire", 2000, 6500, "2020-12-01"],
     666, {
    "marque": "Chevrolet",
    "modele": "Impala",
    "couleur": "Noire",
    "annee": 2000,
    "prix": 6500,
    "date_arrivee": "2020-12-01",
    "vendue": False,
    "date_vendue": ""
    })
])
def test_creer_dict_voiture(monkeypatch, input_clavier, no_inventaire, dict_voiture):
    inputs = iter(input_clavier)
    monkeypatch.setattr('builtins.input', lambda _: next(inputs))
    assert fn_inventaire.creer_dict_voiture() == (no_inventaire, dict_voiture)



