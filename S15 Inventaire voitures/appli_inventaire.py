# Code créé par Rémy Corriveau, 2022-11-21

import fn_inventaire
from pprint import pprint
from textwrap import dedent
from colorama import Fore, init

init(autoreset=True)


def menu_principal() -> str:
    """
    Menu initial, choisir de qu'on fait.

    :return: string du choix valide. [1, 2, 3, 4]
    """
    # Affichage des choix du menu
    print(dedent(f"""
    Bonjour et bienvenu au \"{Fore.BLUE}Meilleur inventaire{Fore.RESET}\"
    
    1- Afficher l'inventaire
    2- Ajouter une voiture
    3- Vendre une voiture
    4- Recherche de voiture
    """))

    while True:  # Validation du choix
        choix = input("Que voulez faire aujourd'hui ? ")
        if choix not in ("1", "2", "3", "4"):  # Choix non valide
            print(f"{Fore.RED}Vous devez entrer un choix entre 1, 2, 3 et 4. Réessayez.\n")
        else:  # Choix valide
            break

    return choix


def main():
    choix = menu_principal()

    if choix == "1":  # Afficher l'inventaire
        fn_inventaire.afficher_inventaire()
    elif choix == "2":  # Ajouter voiture
        no_inventaire, dict_voiture = fn_inventaire.creer_dict_voiture()
        confirmation = fn_inventaire.ajouter_voiture(no_inventaire, dict_voiture)
        print(confirmation)
    elif choix == "3":  # Enlever voiture
        while True:
            no_inventaire = input("\nVeuillez entrer le numéro d'inventaire de la voiture qui a été vendue : ")
            if fn_inventaire.no_inventaire_disponible(no_inventaire):  # Numéro non assigné
                print(f"{Fore.RED}Ce numéro n'est pas dans la liste. Réessayez.")
            else:  # Numéro assigné
                break
        confirmation = fn_inventaire.vendre_voiture(no_inventaire)
        print(confirmation)
    elif choix == "4":  # Recherche
        champ = input("Veuillez entrer la catégorie de recherche [annee, marque, modele, couleur, vendue] : ").lower()
        caracterisque = input("Veuillez entrer la caractéristique recherchée : ")
        resultat = fn_inventaire.recherche_voiture(champ, caracterisque)
        pprint(resultat)


if __name__ == '__main__':
    main()
