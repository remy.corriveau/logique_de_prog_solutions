import json
import re  # Utilisation d'expression régulière pour la validation de la date
import datetime as dt
from datetime import datetime
from pprint import pprint  # Affichage plus joli des dict en console
from textwrap import dedent
from colorama import Fore, init
from typing import List, Tuple, NoReturn

init(autoreset=True)


def lire_inventaire() -> dict:
    """
    Lis inventaire.json et retourne sont contenu.

    :return: dict
    """
    with open("inventaire.json", "r", encoding="utf-8") as inventaire:
        return json.load(inventaire)


def ecrire_inventaire(p_dict: dict) -> NoReturn:
    """
    Écris le dictionnaire dans inventaire.json.

    :param p_dict: Dictionnaire à écrire.
    """
    with open("inventaire.json", "w", encoding="utf-8") as inventaire:
        json.dump(p_dict, inventaire, ensure_ascii=False, indent=4)


def afficher_inventaire() -> NoReturn:
    """
    Affiche le contenu de inventaire.json
    """
    dt_inventaire = lire_inventaire()
    pprint(dt_inventaire)


def no_inventaire_disponible(p_no_inventaire: str) -> bool:
    """
    Vérifie si le numéro est déjà utilisé dans l'inventaire.

    :params p_no_inventaire: Numéro à vérifier.
    :return: True, il est dispo. False, il est pris.
    """
    with open(file="inventaire.json", mode="r", encoding="utf-8") as f:
        dt_inventaire = json.load(f)

    return p_no_inventaire in dt_inventaire.keys()


def creer_dict_voiture() -> Tuple[str, dict]:
    """
    Pose les questions et crée un dict de voiture.

    :return: dict avec toutes les caractéristiques d'une voiture pour l'inventaire.
    """
    # Lire toutes les caractéristiques d'une voiture

    # Calculer le prochain numéro d'inventaire
    dt_inventaire = lire_inventaire()
    dernier_num_inventaire = list(dt_inventaire.keys())[-1]
    prochain_num_inventaire = int(dernier_num_inventaire) + 1

    while True:
        no_inventaire = input(dedent(f"""
        Entrez le numéro d'inventaire pour la nouvelle voiture, 
        laissez vide pour le prochain numéro dans l'ordre [{prochain_num_inventaire:03}] : """))
        if not no_inventaire_disponible(no_inventaire):  # Le numéro est déjà pris
            print(f"{Fore.RED}Ce numéro est déjà pris. Réessayez.")
            continue
        elif no_inventaire == "":
            no_inventaire = f"{prochain_num_inventaire:03}"
        elif not no_inventaire.isdigit():  # Numéro n'est pas des chiffres.
            print(f"{Fore.RED}Le numéro doit être un entier. Réessayez.")
            continue
        break  # Le numéro est valide et disponible

    marque = input("\nEntrez la marque de la voiture : ")
    modele = input("\nEntrez le modèle de la voiture : ")
    couleur = input("\nEntrez la couleur de la voiture : ")

    while True:  # Validation année
        try:
            annee = int(input("\nEntrez l'année de la voiture [format AAAA >= 2000] : "))
            if annee < 2000:
                raise ValueError
        except ValueError:
            print(f"{Fore.RED}L'année doit être un entier plus grand que 1999. Réessayez.")
        else:
            break

    while True:  # Validation prix
        try:
            prix = float(input("\nEntrez le prix de vente de la voiture : "))
        except ValueError:
            print(f"{Fore.RED}Le prix doit être un nombre. Réessayez.")
        else:
            break

    while True:  # Validation date
        date_aujourdhui = f"{datetime.now().date():%Y-%m-%d}"
        date_arrivee = input(dedent(f"""
        Entrez la date d'ajout dans l'inventaire au format AAAA-MM-JJ et minimum en 2020,
        laissez vide pour la date d'aujourd'hui [{date_aujourdhui}] : """))
        if date_arrivee == "":  # Date d'aujourd'hui
            date_arrivee = date_aujourdhui
        elif re.match(r"\d+-\d+-\d+", date_arrivee):  # Format valide
            ls_dates = date_arrivee.split("-")
            if not 2020 <= int(ls_dates[0]) <= int(date_aujourdhui.split("-")[0]):  # Vérification année
                print(f"{Fore.RED}L'année que vous avez entrée est invalide. Réessayez.")
                continue
            elif not 1 <= int(ls_dates[1]) <= 12:  # Vérification mois
                print(f"{Fore.RED}Le mois que vous avez entrée est invalide. Réessayez.")
                continue
            elif not 1 <= int(ls_dates[2]) <= 31:  # Vérification journée
                print(f"{Fore.RED}La journée que vous avez entrée est invalide. Réessayez.")
                continue
        else:  # Date non valide
            print(f"{Fore.RED}La date que vous avez entrez n'est pas au format AAAA-MM-JJ. Réessayez.")
            continue
        # Toutes les conditions sont valides.
        break

    return no_inventaire, {
        "marque": marque,
        "modele": modele,
        "couleur": couleur,
        "annee": annee,
        "prix": prix,
        "date_arrivee": date_arrivee,
        "vendue": False,  # Une nouvelle voiture n'est jamais déjà vendue
        "date_vendue": ""
    }


def ajouter_voiture(p_no_inventaire: str, p_dict_voiture: dict) -> str:
    """
    Ajoute une voiture au fichier inventaire.json.

    :param p_no_inventaire: Clé pour la nouvelle entrée.
    :param p_dict_voiture: Valeur pour la nouvelle entrée.
    :return: Message de confirmation.
    """
    # Lire inventaire.json
    dt_inventaire = lire_inventaire()

    # Ajouter la nouvelle voiture au dictionnaire
    dt_inventaire[p_no_inventaire] = p_dict_voiture

    # Écrire le dictionnaire modifié dans inventaire.json
    ecrire_inventaire(dt_inventaire)

    return f"\nLa voiture {Fore.BLUE}{p_no_inventaire}{Fore.RESET} de la marque \
{Fore.BLUE}{dt_inventaire[p_no_inventaire]['marque']}{Fore.RESET} a bien été ajoutée."


def vendre_voiture(p_no_inventaire: str) -> str:
    """
    Modifie l'inventaire pour marquer une voiture comme vendu avec la date d'aujourd'hui.

    :param p_no_inventaire:
    :return: Message de confirmation et durée de l'auto dans l'inventaire.
    """
    # Lire inventaire.json
    dt_inventaire = lire_inventaire()

    # Vérifier si elle est déjà vendue
    if dt_inventaire[p_no_inventaire]["vendue"]:
        return f"{Fore.RED}La voiture {p_no_inventaire} est déjà vendue. Rien n'a été fait."
    else:
        # Marquer la voiture comme vendue
        dt_inventaire[p_no_inventaire]["vendue"] = True

        # Inscrire la date de vente
        date_vendue = datetime.now().date()
        dt_inventaire[p_no_inventaire]["date_vendue"] = f"{date_vendue:%Y-%m-%d}"

        # Écrire le dictionnaire modifié dans inventaire.json
        ecrire_inventaire(dt_inventaire)

        # Calculer la durée en inventaire
        str_date_arrivee = dt_inventaire[p_no_inventaire]["date_arrivee"].split("-")  # Format 2022-11-25
        date_arrivee = dt.date(year=int(str_date_arrivee[0]), month=int(str_date_arrivee[1]), day=int(str_date_arrivee[2]))
        duree_en_inventaire = (date_vendue - date_arrivee).days # Calculer le "timedelta", la différence entre les dates

        return dedent(f"""
        La voiture {p_no_inventaire} a été marqué comme vendue. 
        Elle est resté en inventaire pendant {duree_en_inventaire} jours.""")


def recherche_voiture(p_cle: str, p_caracteristique) -> List[dict]:
    """
    Cherche tous les voitures dans la liste qui correspondent au critère.

    :param p_cle: Dans quel champ faire la recherche.
    :param p_caracteristique: Ce qu'on recherche.
    :return: dict de toutes les voitures qui correspondent.
    """
    # Lire inventaire.json
    dt_inventaire = lire_inventaire()

    # Faire la recherche dans l'inventaire
    ls_recherchee = []
    for voiture in dt_inventaire.keys():
        # Je mets en string au cas où on recherche par l'année qui est un int.
        if str(dt_inventaire[voiture][p_cle]).lower() == str(p_caracteristique).lower():
            ls_recherchee.append(dt_inventaire[voiture])
    return ls_recherchee

