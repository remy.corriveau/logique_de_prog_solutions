import unidecode
import re


def standard_string(chaine: str) -> str:
    """
    Reçoit une chaîne de caractère, enlève les espaces de début et de fin,
    remplace tout les caractères accentués par leur équivalent sans accent et
    met le tout en minuscule.

    :param chaine: Chaîne de caractère à standardiser.
    :return: Chaîne sans accent et en minuscule.
    """
    return unidecode.unidecode(chaine.strip().lower())


def validation_nom(chaine: str) -> bool:
    """
    Vérifie que la chaîne de caractère contient seulement des lettres, tirets et apostrophes.

    :param chaine: Nom à vérifier.
    :return: Vrai ou faux.
    """
    nom_valide = True
    for car in chaine:
        if not nom_valide:  # Déjà un caractère fautif
            break
        # Caractères acceptés ["'", "-", " ", {alpha}]
        if car not in ["\'", "-", " "] and not car.isalpha():
            nom_valide = False

    return nom_valide


def validation_nom_regex(chaine: str) -> bool:
    """
    Vérifie que la chaîne de caractère contient seulement des lettres, tirets et apostrophes.

    :param chaine: Nom à vérifier.
    :return: Vrai ou faux.
    """
    regex = r"(?:[a-zÀ-ÿ '-])+"
    if re.fullmatch(regex, chaine, re.IGNORECASE) is not None:
        return True
    else:
        return False
