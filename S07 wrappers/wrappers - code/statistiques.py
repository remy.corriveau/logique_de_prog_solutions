import statistics


def calculer_moyenne(ls_nombres: list) -> float:
    """Calcule la moyenne à partir d'une liste de nombre.

    :param ls_nombres: Une liste de nombre.
    :return: la moyenne de tous les nombres.
    :raises TypeError: La liste n'est pas valide.
    """
    for nombre in ls_nombres:
        if type(nombre) not in (float, int):
            raise TypeError(f"Votre liste n'est pas valide. {nombre} n'est pas un nombre.")
    return statistics.mean(ls_nombres)


def calculer_mediane(ls_nombres: list) -> float:
    """Calcule la médiane à partir d'une liste de nombre.

    :param ls_nombres: Une liste de nombre.
    :return: la moyenne de tous les nombres.
    :raises TypeError: La liste n'est pas valide.
    """
    for nombre in ls_nombres:
        if not isinstance(nombre, (int, float)):
            raise TypeError(f"Votre liste n'est pas valide. {nombre} n'est pas un nombre.")
    return statistics.median(ls_nombres)
