# Variables
armee_galadriel = [250, 150, 500]
armee_sauron = [100, 200, 2500]

# CALCULER la puissance totale de chaque armée en force humaine et orc
force_galadriel = (armee_galadriel[0] * 10) + (armee_galadriel[1] * 5) + armee_galadriel[2]
force_sauron = (armee_sauron[0] * 10) + (armee_sauron[1] * 5) + armee_sauron[2]

# LIRE la stratégie choisie par Galadriel
strategie = input("""
Bienvenu Galadriel, à votre simulateur de combat.
Quelle stratégie voulez-vous adopter [aggro, def] ? """)

# CALCULER la nouvelle puissance des forces en fonction de la stratégie
if strategie == "aggro":
    force_sauron = force_sauron - 500
else:
    force_galadriel = force_galadriel + (175 * 5)

# AFFICHER le résultat de la bataille
if force_galadriel >= force_sauron:
    print("\nLa bataille est remportée et la paix pourra de nouveau régner.")
else:
    print("\nLes forces du bien s'inclinent et la terre du milieu sombre dans le chaos.")
