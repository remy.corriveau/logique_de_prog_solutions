# Déclaration de variables
ls_mots = ["Nicolas", "chocolat.", "beau", "aime", "Le", "le"]

# Enlever "beau"
ls_mots.remove("beau")

# Ajouter "grand"
ls_mots.insert(2, "grand")

# Trier la liste en ordre croissant.
ls_mots.sort()

# Mettre "chocolat." en majuscule
position_choco = ls_mots.index("chocolat.")
ls_mots.insert(position_choco, ls_mots.pop(position_choco).upper())

# Afficher la phrase "Le grand Nicolas aime le chocolat."
print(f"\n{ls_mots[0]} {ls_mots[4]} {ls_mots[1]} {ls_mots[2]} {ls_mots[5]} {ls_mots[3]}")
