from getpass import getpass
from textwrap import dedent
from datetime import datetime
from colorama import Fore, init
init(autoreset=True)


def verifier_mot_de_passe(mot_de_passe: str, derniere_mod: str = None) -> bool:
    """
    Vérifier si le mot de passe respecte les critères de sécurité.
    Longeur de 6 caractères, avec au moins une majuscule, une minuscule et un chiffre.
    :param mot_de_passe: Le mot de passe à vérifier
    :param derniere_mod: La date de dernière modification du mot de passe, optionnel
    :return: True si le mot de passe est valide, False sinon
    """
    longueur, majuscule, minuscule, chiffre, chiffre_debut, interval = False, False, False, False, False, False

    if len(mot_de_passe) >= 6:
        longueur = True

    if not mot_de_passe[0].isdigit():
        chiffre_debut = True

    if derniere_mod:
        interval = datetime.now() - datetime.strptime(derniere_mod,"%Y-%m-%d %H:%M:%S") > timedelta(minutes=5)
    else:
        interval = True

    for char in mot_de_passe:
        if char.isupper():
            majuscule = True
        elif char.islower():
            minuscule = True
        elif char.isdigit():
            chiffre = True

    if not longueur:
        print(f"{Fore.RED}Le mot de passe doit contenir au moins 6 caractères.")
    if not chiffre_debut:
        print(f"{Fore.RED}Le mot de passe doit commencer par un chiffre.")
    if not interval:
        print(f"{Fore.RED}Le mot de passe a été changé il y a moins de 5 minutes, veuillez attendre.")
    if not majuscule:
        print(f"{Fore.RED}Le mot de passe doit contenir au moins une majuscule.")
    if not minuscule:
        print(f"{Fore.RED}Le mot de passe doit contenir au moins une minuscule.")
    if not chiffre:
        print(f"{Fore.RED}Le mot de passe doit contenir au moins un chiffre.")

    return longueur and majuscule and minuscule and chiffre and chiffre_debut and interval


def ajouter_employe(dt_employe: dict) -> dict:
    """
    Créer un nouvel employé, avec un mot de passe valide.
    :return: Le nom d'usager et les infos de mot de passe de l'employé
    """
    # Vérifier si l'employé existe déjà
    nom_utilisateur = input("Entrez le nom de l'employé : ")
    if nom_utilisateur in dt_employe:
        print(f"{Fore.RED}L'employé existe déjà.")
        return dt_employe
    elif nom_utilisateur == "":
        print(f"{Fore.RED}Une chaîne vide n'est pas un nom d'employé valide.")
        return dt_employe

    # Vérifier si le mot de passe est valide
    mot_de_passe = getpass("Entrez le mot de passe de l'employé : ")
    while not verifier_mot_de_passe(mot_de_passe):
        mot_de_passe = getpass("\nEntrez le mot de passe de l'employé : ")

    # Ajouter le nouvel employé à la base de données
    nouvel_employe = {nom_utilisateur: {"mdp": mot_de_passe, "derniere_mod": datetime.now().strftime("%Y-%m-%d %H:%M:%S")}}
    dt_employe.update(nouvel_employe)
    print(f"{Fore.GREEN}Employé ajouté avec succès !")
    return dt_employe


def modifier_mot_de_passe(dt_employes: dict) -> dict:
    """
    Modifier le mot de passe d'un employé existant.
    :param dt_employes: La base de données des employés
    :return: La base de données des employés, avec le mot de passe modifié
    """
    # Vérifier si l'employé existe
    nom_utilisateur = input("Entrez le nom de l'employé : ")
    if nom_utilisateur not in dt_employes:
        print(f"{Fore.RED}L'employé n'existe pas.")
        return dt_employes

    # Vérifier si l'ancien mot de passe est valide
    ancien_mot_de_passe = getpass("Entrez l'ancien mot de passe de l'employé : ")
    if ancien_mot_de_passe != dt_employes[nom_utilisateur]["mdp"]:
        print(f"{Fore.RED}Le mot de passe est incorrect.")
        return dt_employes

    # Demander le nouveau mot de passe
    mot_de_passe = getpass("Entrez le nouveau mot de passe de l'employé : ")
    # Vérifier si le nouveau mot de passe est valide
    while not verifier_mot_de_passe(mot_de_passe):
        mot_de_passe = getpass("Entrez le nouveau mot de passe de l'employé : ")

    # Modifier le mot de passe de l'employé
    dt_employes[nom_utilisateur]["mdp"] = mot_de_passe
    dt_employes[nom_utilisateur]["derniere_mod"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    print(f"{Fore.GREEN}Mot de passe modifié avec succès !")
    return dt_employes


def afficher_employes(dt_employes: dict):
    """
    Afficher la base de données des employés.
    :param dt_employes: La base de données des employés
    """
    print(f'{Fore.BLUE}{"Utilisateur":<20}{"Mot de passe":<20}{"Dernière modification":<20}')
    for nom, infos in dt_employes.items():
        print(f"{nom:<20}{infos['mdp']:<20}{infos['derniere_mod']:<20}")
