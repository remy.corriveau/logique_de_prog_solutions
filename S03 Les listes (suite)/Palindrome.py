# Créer la liste initiale
ls_nombres = [71, 55, 30, 42, 25, 68, 15]
print(f"La liste de départ : {ls_nombres}\n")

# Vérifiez si l'élément à l'indice 2 de la liste est divisible par 5.
divisible_par_5 = ls_nombres[2] % 5 == 0
print(f"L'élément à la position 2 de la liste est divisible par 5 : {divisible_par_5}")

# Vérifiez si le dernier élément de la liste est pair.
est_pair = ls_nombres[-1] % 2 == 0
print(f"L'élément à la dernière position est pair : {est_pair}")

# Vérifiez si l’élément 55 de la liste est impair.
est_impair = ls_nombres[ls_nombres.index(55)] % 2 != 0
print(f"L'élément 55 de la liste est impair : {est_impair}")

# Incrémentez le premier élément de la liste (à la position 0) de 100.
valeur_plus_100 = ls_nombres[0] + 100
ls_nombres[0] = valeur_plus_100
print(f"La nouvelle valeur à la position 0 est : {ls_nombres[0]}")

# Échangez les positions des valeurs 30 et 68 dans la liste.
position_30 = ls_nombres.index(30)
position_68 = ls_nombres.index(68)
ls_nombres[position_30] = 68
ls_nombres[position_68] = 30
print(f"La liste avec les valeurs 30 et 68 inversées : {ls_nombres}")

# Triez la liste.
liste_croissante = sorted(ls_nombres)
print(f"La liste en ordre croissant : {liste_croissante}")

# Inversez la liste.
liste_inverse = list(reversed(ls_nombres))
print(f"La liste en ordre inverse : {liste_inverse}")

# Divisez la liste en deux parties pour créer deux listes distinctes.
position_milieu = len(ls_nombres) // 2
liste_partie1 = ls_nombres[0:position_milieu]
liste_partie2 = ls_nombres[position_milieu:-1]

print(f"""
La première partie de la liste : {liste_partie1}
La deuxième partie de la liste : {liste_partie2}
""")

