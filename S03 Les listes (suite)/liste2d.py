# Sachant que :
# •	Les éléments aux indices 0 à 4 sont des langages de programmation.
# •	Les éléments aux indices 5 à 8 sont des Bases de données.
# •	Les éléments aux indices 9 à 12 sont des technologies web.
# •	Les éléments aux indices 13 à 17 sont des systèmes d’exploitation.
#
# Créez une nouvelle liste contenant des éléments qui sont eux-mêmes des listes.
# Ces listes sont, respectivement, celles des langages de programmation, des bases de données,
# des technologies web et des systèmes d’exploitation.
#
# Liste à deux dimensions attendues :
# [["Python", "JavaScript", "Java", "C++", "C#"],
# ["MySQL", "PostgreSQL", "MongoDB", "Oracle"],
# ["HTML", "CSS", "React", "Node.js"],
# ["Linux", "Windows", "macOS", "iOS", "Android"]]
#
# Affichez chacune des sous listes de technologies.

# Liste initiale
technologies = ["Python", "JavaScript", "Java", "C++", "C#",
                "MySQL", "PostgreSQL", "MongoDB", "Oracle",
                "HTML", "CSS", "React", "Node.js",
                "Linux", "Windows", "macOS", "iOS", "Android"]

# Convertir en liste 2d
types_technologies = ["Langages de programmation", "Bases de données", "Technologies web", "Systèmes d’exploitation"]
ls2d_technologies = [technologies[:5], technologies[5:9], technologies[9:13], technologies[13:]]

# Afficher les sous-listes
plus_long = max(len(types_technologies[0]), len(types_technologies[1]), len(types_technologies[2]), len(types_technologies[3]))
print(f"""
{types_technologies[0]:{plus_long}} : {ls2d_technologies[0]}
{types_technologies[1]:{plus_long}} : {ls2d_technologies[1]}
{types_technologies[2]:{plus_long}} : {ls2d_technologies[2]}
{types_technologies[3]:{plus_long}} : {ls2d_technologies[3]}
""")




