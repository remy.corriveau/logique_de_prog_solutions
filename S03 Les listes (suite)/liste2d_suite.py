# Faites la même chose que l'exercice 5 avec la liste suivante qui est légèrement différente de la précédente.
# À vous de remarquer les différences.
# technologies = ["Langages de programmation", "Python", "JavaScript", "Java", "C++", "C#",
#                 "Bases de données", "MySQL", "PostgreSQL", "MongoDB", "Oracle",
#                 "Web", "HTML", "CSS", "React", "Node.js",
#                 "Systèmes d'exploitation", "Linux", "Windows", "macOS", "iOS", "Android"]

# Liste à utiliser
technologies = ["Langages de programmation", "Python", "JavaScript", "Java", "C++", "C#",
                "Bases de données", "MySQL", "PostgreSQL", "MongoDB", "Oracle",
                "Web", "HTML", "CSS", "React", "Node.js",
                "Systèmes d'exploitation", "Linux", "Windows", "macOS", "iOS", "Android"]

# Convertir en liste 2d
ls2d_technologies = [technologies[:6], technologies[6:11], technologies[11:16], technologies[16:]]

# Afficher les sous-listes
print(f"""
{ls2d_technologies[0][0]} : {ls2d_technologies[0][1:]}
{ls2d_technologies[1][0]} : {ls2d_technologies[1][1:]}
{ls2d_technologies[2][0]} : {ls2d_technologies[2][1:]}
{ls2d_technologies[3][0]} : {ls2d_technologies[3][1:]}
""")
