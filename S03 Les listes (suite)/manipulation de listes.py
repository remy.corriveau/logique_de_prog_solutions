# Déclaration de variable
palindrome = "Un radata na."

# Conversion de la chaîne en liste.
liste_palindrome = list(palindrome)

# Remplacez le premier élément "U" par "u".
liste_palindrome[0] = "u"

# Remplacez le 8ème élément de la liste par "r".
liste_palindrome[7] = "r"

# Remplacez le 4ème "a" de la liste par "u".
liste_palindrome[-2] = "u"

# Supprimez le caractère "."
liste_palindrome.remove(".")

# Supprimez le 3ème "a" de la liste.
liste_palindrome.pop(8)

# Supprimez tous les caractères espaces de la liste.
qt_espaces = liste_palindrome.count(" ")
for i in range(qt_espaces):
    liste_palindrome.remove(" ")

# Créez une nouvelle liste appelée liste_palindrome_inversee
liste_palindrome_inversee = list(reversed(liste_palindrome))

# Comparez les deux listes
print(f"\"{''.join(liste_palindrome)}\" est un palindrome : {liste_palindrome == liste_palindrome_inversee}")
