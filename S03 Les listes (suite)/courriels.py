# Créez un programme qui demande à l’utilisateur d’entrer trois fois un nom et un prénom
# que vous devez mettre dans une liste. Il doit également demander le nom de domaine
# à utiliser pour les trois personnes (par exemple: gmail.com, cegepoutauais.qc.ca, hotmail.com, etc.).
#
# Le programme doit transformer les noms, prénoms et nom de domaine
# en trois adresses courriel au format suivant : [prenom].[nom]@[domaine]

# Lire les noms et le domaine à utiliser
user1 = input("Entrez le prénom et le nom de la première personne (Ex. Rémy Corriveau)  : ")
user2 = input("Entrez le prénom et le nom de la deuxième personne (Ex. Rémy Corriveau)  : ")
user3 = input("Entrez le prénom et le nom de la troisième personne (Ex. Rémy Corriveau) : ")
domaine = input("Entrez le nom de domaine à utiliser (Ex. gmail.com) : ")

# Générer les courriels
user1_avec_domaine = user1.replace(" ", ".").lower() + "@" + domaine
user2_avec_domaine = user2.replace(" ", ".").lower() + "@" + domaine
user3_avec_domaine = user3.replace(" ", ".").lower() + "@" + domaine

# Afficher les courriels
print(f"""
Courriel de la première personne  : {user1_avec_domaine}
Courriel de la deuxième personne  : {user2_avec_domaine}
Courriel de la troisième personne : {user3_avec_domaine}
""")
