def creer_banniere(msg: str, symbole: str) -> str:
    """
    Créer une bannière à partir d'un message et d'un symbole
    :param msg: Le message à encadrer
    :param symbole: Le symbole qui encadre le message
    :return: La bannière construite
    """
    ligne_message = f"{symbole} {msg} {symbole}"
    longueur_symbole = symbole * len(ligne_message)
    # Si le nombre de symboles n'arrive pas exactement avec la longueur de la ligne, on coupe.
    ligne_haut_bas = longueur_symbole[:len(ligne_message)]
    banniere = f"{ligne_haut_bas}\n{ligne_message}\n{ligne_haut_bas}"
    return banniere
