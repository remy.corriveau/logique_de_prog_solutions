def separer_division(dividende: float, diviseur: float) -> tuple[int, int, float]:
    """
    Sépare le dividende par le diviseur et retourne une liste de trois éléments.
    :param dividende: Le nombre à diviser.
    :param diviseur: Le nombre par lequel on divise.
    :return: Tuple, contenant le quotient, le reste et le résultat de la division.
    """
    return int(dividende // diviseur), int(dividende % diviseur), dividende / diviseur
