def inverser_valeur(liste: list, val1: any, val2: any) -> list:
    """
    Inverse les valeurs val1 et val2 dans la liste.
    :param liste: La liste dans laquelle on veut inverser les valeurs
    :param val1: Première valeur à inverser
    :param val2: Deuxième valeur à inverser
    :return: La liste modifiée.
    """
    # Trouver les positions des deux valeurs
    index_val1 = liste.index(val1)
    index_val2 = liste.index(val2)

    # Inverser les valeurs
    liste[index_val1], liste[index_val2] = liste[index_val2], liste[index_val1]
    return liste
