import banniere
import inverser_valeurs
import inverser_positions
import separer_division

# On utilise les modules ici

# banniere
message = input("Entrez un message: ")
symbole = input("Entrez un symbole: ")
banniere = banniere.creer_banniere(message, symbole)
print(f"\n{banniere}")

# inverser_valeurs
la_liste = [1, 2, 3, 4, 5]
valeur1 = 2
valeur2 = 4
liste_inversee = inverser_valeurs.inverser_valeur(la_liste[:], valeur1, valeur2)
print(f"\nLes valeurs 2 et 4 ont été inversées.\n"
      f"{la_liste} -> {liste_inversee}")

# inverser_positions
la_liste = [1, 2, 3, 4, 5]
position1 = 2
position2 = 4
liste_inversee = inverser_positions.inverser_position(la_liste[:], position1, position2)
print(f"\nLes positions 2 et 4 ont été inversées.\n"
      f"{la_liste} -> {liste_inversee}")

# separer_division
dividende = float(input("\nQuel est le dividende : "))
diviseur = float(input("Quel est le diviseur : "))
quotient, reste, resultat = separer_division.separer_division(dividende, diviseur)
print(f"""
Pour la division de {dividende} par {diviseur} :
Le quotient est {quotient}
Le reste est    {reste}
Le résultat est {resultat}
""")

