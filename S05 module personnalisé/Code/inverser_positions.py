def inverser_position(liste: list, pos1: int, pos2: int) -> list:
    """
    Inverse les valeurs aux positions passées en paramètre dans la liste.
    :param liste: La liste dans laquelle on veut inverser les valeurs
    :param pos1: Première valeur à inverser
    :param pos2: Deuxième valeur à inverser
    :return: La liste modifiée.
    """
    # Inverser les valeurs
    liste[pos1], liste[pos2] = liste[pos2], liste[pos1]
    return liste
