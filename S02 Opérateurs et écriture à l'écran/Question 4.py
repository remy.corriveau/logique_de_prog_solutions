# Lire les nombres
nb1 = float(input("Entrer le premier nombre : "))
nb2 = float(input("Entrer le deuxième nombre : "))

# Convertir en int si possible (optionnel)
if nb1.is_integer():
    nb1 = int(nb1)
if nb2.is_integer():
    nb2 = int(nb2)

# Afficher les nombres avec tous les opérateurs
print(f"""Opérateurs arithmétiques
{nb1} +  {nb2} = {nb1 +  nb2}
{nb1} -  {nb2} = {nb1 -  nb2}
{nb1} *  {nb2} = {nb1 *  nb2}
{nb1} /  {nb2} = {nb1 /  nb2}
{nb1} // {nb2} = {nb1 // nb2}
{nb1} %  {nb2} = {nb1 %  nb2}

Opérateurs logiques
{nb1} == {nb2} = {nb1 == nb2}
{nb1} != {nb2} = {nb1 != nb2}
{nb1} <  {nb2} = {nb1 <  nb2}
{nb1} <= {nb2} = {nb1 <= nb2}
{nb1} >  {nb2} = {nb1 >  nb2}
{nb1} >= {nb2} = {nb1 >= nb2}
""")
