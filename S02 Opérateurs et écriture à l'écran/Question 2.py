import math

# Recueillir les données
nom = input("Quel est votre nom ? ")
no = input("Quel est votre numéro d'étudiant ? ")
date = input("Quelle est votre date de naissance ? ")
symbole = input("\nQuel symbole voulez-vous pour encadrer ? ")

# Préparer les débuts de lignes
prefix_nom = f"{symbole * 2}  Nom: "
prefix_no = f"{symbole * 2}  No étudiant: "
prefix_date = f"{symbole * 2}  Date: "

###############SECTION OPTIONNELLE############################
# Trouver la plus longue chaîne
longueur_ligne_nom = len(nom) + len(prefix_nom)
longueur_ligne_no = len(no) + len(prefix_no)
longueur_ligne_date = len(date) + len(prefix_date)
plus_longue_ligne = longueur_ligne_nom

if plus_longue_ligne < longueur_ligne_no:
    plus_longue_ligne = longueur_ligne_no

if plus_longue_ligne < longueur_ligne_date:
    plus_longue_ligne = longueur_ligne_date

# Ajuster la longeur de la ligne si le symbole a plusieurs caractères de long
plus_longue_ligne =math.ceil(plus_longue_ligne / len(symbole))
################################################################

# Affichage
print(f"""
{symbole * plus_longue_ligne}
{prefix_nom + nom}
{prefix_no + no}
{prefix_date + date} 
{symbole * plus_longue_ligne}
""")
