# Créer la liste de triangles
ls_triangles = [[3, 4, 5], [5, 12, 12], [29, 420, 421], [24, 143, 170], [52, 73, 93]]

# Affichage des triangles
print("""
Est-ce que les triangles suivants sont rectangles ?

Triangle 1 : Côtés de 3, 4 et 5
Triangle 2 : Côtés de 5, 12 et 12
Triangle 3 : Côtés de 29, 420 et 421
Triangle 4 : Côtés de 24, 143 et 170
Triangle 5 : Côtés de 52, 73 et 93
""")

# Boucle pour affichage des réponses (optionnel)
compteur = 1
for triangle in ls_triangles:
    print(f"Triangle {compteur} : {triangle[0] ** 2 + triangle[1] ** 2 == triangle[2] ** 2}")
    compteur = compteur + 1  # Incrémentation du compteur
