print("""
**************************************
**  Bienvenue dans mon application  **
**************************************
1. Hello World
0. Quitter
""")

# Lire le choix
choix = input("Veuillez entrer un choix : ")

# Section optionnelle
# Afficher selon le choix.
if choix == "1":
    print("Hello World !")
else:
    print("Bye bye !")
    exit()  # Ferme l'application
