print(f"""
5 * 4 est égal à 2 * 10 ? {5 * 4 == 2 * 10}
52 * 20 + 4 est plus petit que 130 * 8 ? {52 * 20 + 4 < 130 * 8}
52 * (20 + 4) est plus grand ou égal à 12 * 100 + 4 ? {52 * (20 + 4) >= 12 * 100 + 4}
""")
