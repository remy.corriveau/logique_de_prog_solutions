import sys


def add_nombres(nb1: [int, float], nb2: [int, float] = 5) -> [int, float]:
    """
    Additionne deux nombres et retourne le résultat.
    Si seulement un nombre est entré, le deuxième nombre sera 5.
    :param nb1: Le premier nombre à additionner.
    :param nb2: Le deuxième nombre à additionner.
    :return: Le résultat de l'addition.
    """
    if isinstance(nb1, (int, float)) and isinstance(nb2, (int, float)):
        return nb1 + nb2
    else:
        raise TypeError


if __name__ == "__main__":
    # LIRE les nombres à additionner CALCULER le résultat de l'addition.
    try:
        nombre1 = float(input("Premier nombre à additionner : "))
    except ValueError:
        sys.exit("Le nombre doit être un nombre réel.")

    entree2 = input("Voulez-vous entrer un deuxième nombre, sinon sa valeur par défaut sera de 5 [oui, non] ?")
    if entree2.lower().strip() in ["oui", "o", "y", "yes"]:
        try:
            nombre2 = float(input("Deuxième nombre à additionner : "))
        except ValueError:
            sys.exit("Le nombre doit être un nombre réel.")
        try:
            resultat = add_nombres(nombre1, nombre2)
        except TypeError:
            sys.exit("Les nombres doivent être des réels.")
    else:
        nombre2 = 5
        try:
            resultat = add_nombres(nombre1)
        except TypeError:
            sys.exit("Le nombre doit être un nombre réel.")

    # AFFICHER le résultat.
    print(f"\n{nombre1} + {nombre2} = {resultat}")
