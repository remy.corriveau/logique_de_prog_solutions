import sys


def comparer_nombres(nombre1: [int, float], nombre2: [int, float]) -> str:
    """
    Compare deux nombres et retourne le résultat.
    :param nombre1: Le premier nombre à comparer.
    :param nombre2: Le deuxième nombre à comparer.
    :return: Une phrase décrivant la comparaison.
    """
    if isinstance(nombre1, (int, float)) and isinstance(nombre2, (int, float)):
        if nombre2 < nombre1:
            return f"{nombre2} est plus petit que {nombre1}."
        elif nombre1 == nombre2:
            return f"{nombre1} est égal à {nombre2}."
        else:
            return f"{nombre2} est plus grand que {nombre1}."
    else:
        raise TypeError


if __name__ == "__main__":
    # Variables
    nombre_debut = 0

    nombre_nouveau = input("\nEntrer un nombre à comparer : ")
    # Boucle jusqu'à ce que l'entrée ne soit pas un nombre réel
    while (nombre_nouveau.count(".") <= 1 and  # 0 ou 1 point
           (nombre_nouveau[0] == "-" or  # 1er caractère est un moins ou un chiffre
           nombre_nouveau[0].isnumeric()) and
           (nombre_nouveau.replace(".", "").isnumeric() or
           nombre_nouveau[1:].replace(".", "").isnumeric())):  # reste des caractères sont des chiffres
        # Convertir en nombre réel.
        nombre_nouveau = float(nombre_nouveau)

        # COMPARER les nombres et AFFICHER le résultat.
        try:
            print(comparer_nombres(nombre_debut, nombre_nouveau))
        except TypeError:
            sys.exit("Les nombres doivent être des réels.")

        # Remplacer nombre de base
        nombre_debut = nombre_nouveau

        # LIRE le nombre à comparer
        nombre_nouveau = input("\nEntrer un nombre à comparer : ")

    # Affichage de sortie
    print("Merci d'avoir joué.")
