def main():
    # Variables
    age = 20
    salaire = 20_000
    pourcent_int = 0.06
    pourcent_eco = 0.20
    pourcent_aug = 0.05
    interets_an = 0
    total_bourse = 0

    while True:
        # Calculer intérêt annuel
        interets_an = total_bourse * pourcent_int

        # Vérifier si intérêt sont suffisants pour la retraite
        if interets_an >= 35_000:
            print("C'est la retraite !")
            break  # Sort de la boucle 'while'
        
        # Travail une année de plus
        else:
            # Ajouter intérêt en bourse
            total_bourse = total_bourse + interets_an

            # Ajouter nouvelles économies en bourse
            total_bourse = total_bourse + (salaire * pourcent_eco)

            # Augmenter salaire
            salaire = salaire + (salaire * pourcent_aug)

            # Vieillir
            age = age + 1
            
    print(f"Nicolas prendra sa retraite à l'âge vénérable de {age}.")


if __name__ == "__main__":
    main()









