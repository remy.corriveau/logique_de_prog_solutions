import sys
from colorama import init, Fore, Style
init(autoreset=True)


def calculer_pourboire(prix_repas: float, pourcent_pourboire: float) -> float:
    """
    Calculer le montant du pourboire à donner au livreur.
    :param prix_repas: Le prix du repas.
    :param pourcent_pourboire: Le pourcentage du pourboire.
    :return: Le montant du pourboire.
    """
    if not isinstance(prix_repas, (float, int)) and not isinstance(pourcent_pourboire, (float, int)):
        raise TypeError("Le prix du repas et le pourboire doivent être des nombres réels.")

    return prix_repas * pourcent_pourboire


def main():
    # VARIABLES :
    prix_poulet = 20.00
    prix_hamburger = 8.50
    prix_burrito = 15.00
    prix_soupe = 13.00
    prix_vegane = 17.50
    prix_livr_skip = 3.00
    prix_livr_dd = 5.00
    prix_livr_uber = 4.00
    montant_pourboire = 0.00

    # AFFICHER menu bouffe
    print(f"""Bonjour Nicolas!
     
    Voici le menu : 
     
    1- Poulet - 20.00 $
    2- Hamburger - 8.50 $
    3- Burrito - 15.00 $
    4- Soupe tonkinoise - 13.00 $
    5- Quelque chose de végane - 17.50 $""")

    while True:
        # LIRE choix de Nicolas pour le menu
        choix_menu = input("\nQue désires-tu manger aujourd’hui [1, 2, 3, 4, 5] ? ")
        if choix_menu in ["1", "2", "3", "4", "5"]:
            break
        else:
            print(f"{Fore.RED}Le choix doit être entre 1 et 5, essayez encore.")

    # LIRE choix de Nicolas pour la pourboire
    try:
        pourcent_pourboire = float(input("\nCombien de pourboire ajoutes-tu à la facture [en pourcentage]? ")) / 100
    except ValueError:
        sys.exit("Le pourboire doit être un nombre réel.")

    # AFFICHER options livraisons
    print(f"""\nLes services de livraisons disponibles sont : 
    SkipTheDishes - 3.00 $
    DoorDash - 5.00 $
    UberEats - 4.00 $ """)

    # LIRE choix de Nicolas pour la livraison
    choix_livraison = input("\nQuel service de livraison préfères-tu [Skip, DD, Uber] ? ")

    # CALCULER la facture
    # Assigner le choix du repas
    if choix_menu == "1":
        repas = "Poulet"
        prix_repas = prix_poulet
    elif choix_menu == "2":
        repas = "Hamburger"
        prix_repas = prix_hamburger
    elif choix_menu == "3":
        repas = "Burrito"
        prix_repas = prix_burrito
    elif choix_menu == "4":
        repas = "Soupe"
        prix_repas = prix_soupe
    else:
        repas = "Végane"
        prix_repas = prix_vegane

    montant_pourboire = calculer_pourboire(prix_repas, pourcent_pourboire)

    # Assigner le choix de livraison
    if choix_livraison.lower() == "skip":
        service_livraison = "SkipTheDishes"
        prix_livraison = prix_livr_skip
    elif choix_livraison.lower() == "dd":
        service_livraison = "DoorDash"
        prix_livraison = prix_livr_dd
    else:
        service_livraison = "UberEats"
        prix_livraison = prix_livr_uber

    # Calculer le total
    total = prix_repas + montant_pourboire + prix_livraison

    # AFFICHER la facture
    print(f"""\nVoici ta facture : 
    {repas:25} {prix_repas:10.2f} $
    {"Pourboire":25} {montant_pourboire:10.2f} $
    {service_livraison:25} {prix_livraison:10.2f} $
     
    {"Total":25} {total:10.2f} $
     
    Merci Nicolas! Le livreur est en route.""")


if __name__ == '__main__':
    main()
