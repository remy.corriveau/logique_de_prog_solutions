def is_ARNm(str_qr: str) -> bool:
    """
    Vérifie le code QR pour savoir si le vaccin est à ARNm.
    :param str_qr: Code QR
    :return: True si le vaccin est à ARNm, False sinon.
    """
    index = 1  # L'index du chiffre après le "8".
    ARNm = True  # On démarre à True et on le met à False dès qu'une valeur n'est pas pair après un 8.
    for caractere in str_qr:
        if caractere == "8":
            if (int(str_qr[index]) % 2) != 0:
                ARNm = False
                break
        index = index + 1

    return ARNm


def main():
    # VARIABLES :
    STR_QR = "1259848064391354397654554421313548276845132247976132023148679865100234367974516412306548678416531205631427999453165056749976541561231564597514"
    age = STR_QR[10:12]
    nb_vaccin = STR_QR.count("8") / 4
    region = STR_QR[-3:]

    # Est-ce que Nicolas est pleinement vacciné ? 2+
    if nb_vaccin >= 2:
        status_vaccin = "Nicolas est pleinement vacciné. :)"
    else:
        status_vaccin = "Nicolas n'est pas pleinement vacciné. :("

    # Est-ce un vaccin à ARN ?
    ARNm = is_ARNm(STR_QR)

    if ARNm:
        type_vaccin = "ARNm"
    else:
        type_vaccin = "NON-ARNm"

    # AFFICHAGE
    print(f"Age de Nicolas          : {age}")
    print(f"Status de vaccination   : {status_vaccin}")
    print(f"Type de vaccin          : {type_vaccin}")
    print(f"Région de Nicolas       : {region}")

    # Séparer la chaine pour modifier l'âge (positions 10, 11) et la région (positions -3:fin)
    str_qr_new = STR_QR[:10] + "32" + STR_QR[12:-3] + "819"
    print(f"\nLa nouvelle chaine qui représente le code QR est : \n{str_qr_new}")


if __name__ == "__main__":
    main()









