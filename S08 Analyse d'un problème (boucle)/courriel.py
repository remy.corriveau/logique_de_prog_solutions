import textwrap  # Pour permettre l'indentation correcte


def remplacer_nowell(texte: str) -> str:
    """
    Remplace “nowell” par “Noël”.
    :param texte: Texte à corriger.
    :return: Texte corrigé.
    """
    return texte.replace("nowell", "Noël")


def remplacer_w_par_e(texte: str) -> str:
    """
    Remplace les “w” par des “e”.
    :param texte: Texte à corriger.
    :return: Texte corrigé.
    """
    return texte.replace("w", "e")


def majuscule_apres_saut_ligne(texte: str) -> str:
    """
    Remplace minuscule par majuscule après chaque saut de ligne.
    :param texte: Texte à corriger.
    :return: Texte corrigé.
    """
    index = 1  # L'index du caractère après le "\n".
    for caractere in texte:
        if caractere == "\n":
            texte = texte[:index] + texte[index:].capitalize()
        index = index + 1
    return texte


def main():
    # VARIABLES :
    texte_courriel = textwrap.dedent("""
    Bonjour maman d’amour!
    jw veux justw tw dirw quw tout va biwn wt wst sous contrôlw.
    commw j’ai été congédié j’ai droit au chômagw wt wn plus un dw mws amis dw Montréal m’a dit qu’il allait mw faire rwntrwr à sa compagniw pour fairw dws jobs wn dwssous dw la tablw.
    jw vais fairw lw doublw d’argwnt d’avant wt jw vais pouvoir mw paywr un trip au Mwxiquw avwc Rachwl pour nowell.
    wllw wst biwn d’accord avwc ça, wllw m’aimw full!
    byw mom,
    ton pwtit garçon chéri,
    nico.
    """)

    # REMPLACER “nowell” par “Noël”.
    texte_courriel_corrige = remplacer_nowell(texte_courriel)

    # REMPLACER les “w” par des “e”.
    texte_courriel_corrige = remplacer_w_par_e(texte_courriel_corrige)

    # REMPLACER minuscule par majuscule après chaque saut de ligne.
    texte_courriel_corrige = majuscule_apres_saut_ligne(texte_courriel_corrige)

    # AFFICHER courriel corrigé.
    print(texte_courriel_corrige)


if __name__ == '__main__':
    main()
