# Files à calculer.
ls_files_attente = [
    ([2, 5, 3, 6, 4], 0), 
    ([2, 5, 3, 6, 4], 1), 
    ([2, 5, 3, 6, 4], 2), 
    ([2, 5, 3, 6, 4], 3), 
    ([2, 5, 3, 6, 4], 4)
]

ls_reponses_prof = [6, 18, 12, 20, 17]  #  Bonnes réponses.
ls_reponses = []

for gens_en_file, position_ami in ls_files_attente:
    # Boucle, calcul du temps d'attente
    temps_attendu = 0
    while gens_en_file[position_ami] > 0:  #  Notre ami n'a pas finit.
        acheteur = (gens_en_file.pop(0) - 1)  #  Un achat de moins à faire.
        if acheteur != 0:
            gens_en_file.append(acheteur)

        temps_attendu += 1
        # Suivre la position de notre ami.
        if position_ami == 0 and acheteur == 0:
            break
        elif position_ami == 0:
            position_ami = len(gens_en_file) - 1
        else:
            position_ami -= 1
    
    ls_reponses.append(temps_attendu)

if ls_reponses_prof == ls_reponses:
    print("Yessir!")
else:
    print("Peut-être tu devrais demander de l'aide.")