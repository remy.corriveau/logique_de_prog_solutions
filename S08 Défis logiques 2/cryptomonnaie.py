ls_a_gerer = [
    (2, 4), (3, 3), (3, 9), 
    (2, 5), (4, 10), (4, 5), 
    (1, 1), (2, 1), (5, 4), (5, 1)
]

ls_reponse_prof = [
    [[0, 1], [2, 3]], 
    [[0], [1], [2]], 
    [[0, 1, 2], [3, 4, 5], [6, 7, 8]], 
    [[0, 1, 2], [3, 4]], 
    [[0, 1, 2], [3, 4, 5], [6, 7], [8, 9]], 
    [[0, 1], [2], [3], [4]], 
    [[0]], 
    [[0], []], 
    [[0], [1], [2], [3], []], 
    [[0], [], [], [], []]
    ]

# Liste réponses cumulatives.
ls_reponse = []

for serveurs, calculs in ls_a_gerer:
    # Liste réponse pour 1 essai.
    ls_servers_loads = []

    # Pour suivre le numéro de calcul à mettre.
    ls_calculs = [x for x in range(calculs)]
    # [0, 1, 2, 3]

    # Ajouter une liste vide pour chaque serveur disponible.
    for i in range(serveurs):
        ls_servers_loads.append([])
        # [[],[]]

    # Combien minimum à tous les serveurs.
    tous_serveurs = calculs // serveurs

    # Combien il en reste d'extra.
    surplus = calculs % serveurs

    # Ajouter les calculs aux serveurs.
    compteur_calcul = 0
    for serveur in ls_servers_loads:
        if surplus > 0:
            surplus -= 1
            for no_calcul in range(tous_serveurs + 1):
                serveur.append(ls_calculs[compteur_calcul])
                compteur_calcul += 1
        else:
            for no_calcul in range(tous_serveurs):
                serveur.append(ls_calculs[compteur_calcul])
                compteur_calcul += 1

    ls_reponse.append(ls_servers_loads)

if(ls_reponse_prof == ls_reponse):
    print("Bravo!")
else:
    print("Tu peux faire mieux.")