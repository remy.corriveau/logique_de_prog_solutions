import fn_chiffrement
import colorama
from colorama import Fore, Back, Style
colorama.init()

def main():
    # Afficher le menu et lire le chiffrement, la chaine et la rotation
    print("""
    Bienvenu à "Meilleur Chiffrement" !

    1- Chiffrer
    2- Déchiffrer
    """)
    while True:
        choix = input("Entrez votre choix ici : ")
        if choix not in ["1", "2"]:
            print(f"{Fore.RED}Votre choix n'est pas valide, veuillez choisir un nombre entre 1 et 2.\n{Fore.RESET}")
        else:
            if choix == "1":
                chiffrer = True
            else:
                chiffrer = False
            break

    chaine_debut = input("Quelle est la chaine à modifier ? ")

    while True:  # Tant que ce n'est pas un nombre entier positif
        try:
            rotation = int(input("Entrez la rotation à utiliser : "))
            if rotation < 0:
                raise ValueError
        except ValueError:
            print(f"{Fore.RED}Votre rotation doit être un nombre entier positif. Essayez encore.\n{Fore.RESET}")
        else:
            break

    # chiffrement et affichage final
    chaine_modifiee = fn_chiffrement.cesar(chaine_debut, rotation, chiffrer)
    print(chaine_modifiee)


if __name__ == '__main__':
    main()
