# Tuple des caractères qui seront décalés.
_tp_decalage = (
    ' ', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
    'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
    't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C',
    'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
    'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
    'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6',
    '7', '8', '9', '!', 'é', 'É', 'À', 'à', 'ù', 'Ù',
    'è', 'È', 'ô', 'Ô', 'Î', 'î', 'ï', 'Ï', "'", '/',
    '$', '%', '?', '&', '*', '(', ')', '_', '+', '=',
    '.', ',', '-', '"', "@")


# Chiffrer et déchiffrer dans la même fonction
def cesar(chaine_debut: str, rotation: int, chiffrer: bool = True) -> str:
    """
    Chiffre une chaine avec la rotation demandée.
    :param chiffrer: True si on chiffre, False, sinon.
    :param chaine_debut: Chaine à modifier.
    :param rotation: Rotation à utiliser.
    :return: Chaine chiffrée.
    """
    longeur_tuple = len(_tp_decalage)
    chaine_chiffree = ""
    for caractere in chaine_debut:
        # Vérifier si le caractère est dans le tuple
        if caractere in _tp_decalage:
            # Trouver la position d'une lettre
            pos_init = _tp_decalage.index(caractere)

            # Ajouter la rotation à la position initiale
            if chiffrer:
                pos_nouv = pos_init + rotation
            else:
                pos_nouv = pos_init - rotation

            # Trouver la nouvelle lettre
            nouveau_car = _tp_decalage[pos_nouv % longeur_tuple]
        else:
            nouveau_car = caractere  # Caractère inchangé

        # Ajouter la nouvelle lettre dans la chaine chiffrée
        chaine_chiffree += nouveau_car

    return chaine_chiffree
