import sys
import textwrap
import random


def statistiques_nombre() -> tuple:
    """
    Demande des nombres et sort sur zéro (0).
    :return: Les plus grands et plus petits nombres entrés.
    """

    # Variables
    grand_nombre = None
    petit_nombre = None

    # Boucle infinie qui demande un nombre
    print(f"\n{'*' * 20}")
    print("Demande des nombres et sort sur zéro (0).\n")
    while True:
        nombre = float(input("Veuillez entrer un nombre : "))

        # Condition de sortie
        if nombre == 0:
            break

        # Stocker le plus petit et le plus grand nombre
        if grand_nombre is None or nombre > grand_nombre:
            grand_nombre = nombre
        if petit_nombre is None or nombre < petit_nombre:
            petit_nombre = nombre

    return grand_nombre, petit_nombre


def trouver_nombre(p_nb: int) -> int:
    """
    Demande de trouver le nombre initial et aide en donnant des indices.
    :param p_nb: Nombre à trouver.
    :return: La quantité de tentatives nécessaires.
    """
    # Variable
    nb_tentatives = 0

    # Boucle infinie qui demande un nombre
    print(f"\n{'*' * 20}")
    print("Demande de trouver le nombre initial et aide en donnant des indices.")
    while True:
        nb_tentatives += 1
        tentative = int(input("\nQuelle est votre meilleure tentative ? "))

        # Condition de sortie
        if p_nb == tentative:
            return nb_tentatives

        if p_nb > tentative:
            print("Trop bas.")
        else:
            print("Trop haut.")


def payer_dette(p_dette: float) -> tuple:
    """
    Déduit les remboursements d'une dette et retourne des statistiques.
    :param p_dette: Montant de la dette initiale.
    :return: Le nombre de paiements et leur moyenne.
    """
    # Variable
    nb_remboursements = 0
    dette_initiale = p_dette

    # Boucle des paiements
    print(f"\n{'*' * 20}")
    print("Déduit les remboursements d'une dette et retourne des statistiques.\n")
    while p_dette > 0:
        remboursement = float(input(f"Dette : {p_dette:<8.2f}$ Combien voulez-vous rembourser ? "))
        if remboursement > p_dette:
            print(f"Dernier remboursement de {p_dette:.2f}$ effectué.")
        p_dette -= remboursement
        nb_remboursements += 1

    print("*" * 20)

    return nb_remboursements, dette_initiale / nb_remboursements


def byebye():
    """ Sortir du programme."""
    print("Au revoir !")
    sys.exit()


def main():
    autre_tour = True
    while autre_tour:

        # Menu
        choix = input(textwrap.dedent(f"""
        Voici les options :    
        1. Statistiques de nombres
        2. Trouver un nombre
        3. Payer une dette
        0. Sortir
        
        Quel est votre choix ? """))

        # Exécuter la fonction selon le choix
        match choix:
            case "1":
                grand_nombre, petit_nombre = statistiques_nombre()
                # Affichage des statistiques
                print(textwrap.dedent(f"""
                Plus grand nombre : {grand_nombre}
                Plus petit nombre : {petit_nombre}
                """))

            case "2":
                nb_a_deviner = random.randint(1, 100)
                nb_tentatives = trouver_nombre(nb_a_deviner)
                print(f"\n{nb_tentatives} tentatives ont été nécessaires.\n")

            case "3":
                dette = float(input("\nQuel est le montant de la dette ? "))
                nb_paiement, moy_paiement = payer_dette(dette)
                print(textwrap.dedent(f"""
                Nombre de paiements :   {nb_paiement:8}
                Moyenne des paiements : {moy_paiement:8.2f}$
                """))

            case _:  # (else) Sortir sur 0 ou choix invalide.
                byebye()

        # Revenir au menu ?
        autre_tour = input("Voulez-vous revenir au menu ? (o/n) ").casefold() in ["o", "oui", "y", "yes"]

    byebye()


if __name__ == '__main__':
    main()
