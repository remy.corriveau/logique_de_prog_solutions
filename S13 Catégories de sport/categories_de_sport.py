import sys
from colorama import Fore, init
init(autoreset=True)


def ajouter_joueur() -> dict or None:
    """
    Ajoute un joueur à la base de données.
    :return: Le nom, l'age et la catégorie du joueur.
    """
    nom = input(f"{Fore.YELLOW}Entrez le nom du joueur: ")
    if nom == "":  # On sort de la boucle si l'utilisateur appuie sur Entrée.
        return None
    while True:
        age = input(f"{Fore.YELLOW}Entrez l'age du joueur: ")
        if age == "":  # On sort de la boucle si l'utilisateur appuie sur Entrée.
            return None
        try:
            age = int(age)
            if age < 3:  # âge minimum pour jouer
                raise ValueError
        except ValueError:
            print(f"{Fore.RED}L'âge doit être un nombre entier supérieur à 3.\n")
        else:
            break
    categorie = selectionner_categorie(age)
    return {nom: [age, categorie]}


def selectionner_categorie(age: int) -> str:
    """
    Sélectionne la catégorie du joueur selon son âge.
    :param age: L'âge du joueur.
    :return: La catégorie du joueur.
    """
    if age <= 4:
        categorie = "Mini-poussins"
    elif age <= 6:
        categorie = "Poussins"
    elif age <= 9:
        categorie = "Benjamins"
    elif age <= 12:
        categorie = "Cadets"
    elif age <= 16:
        categorie = "Élites"
    else:
        categorie = "Adultes"
    return categorie


def tri_age(dt_joueurs: dict, decroissant: bool = False) -> list:
    """
    Retourne une liste des joueurs triée par age croissant ou décroissant.
    :param dt_joueurs: La base de donnée des joueurs.
    :param decroissant: True pour croissant, False pour décroissant.
    :return: Une liste des joueurs triée par age croissant ou décroissant.
    """
    lst_joueurs = []
    for nom, (age, categorie) in dt_joueurs.items():
        lst_joueurs.append((age, nom))
    return sorted(lst_joueurs, reverse=decroissant)


def categorie_joueurs(dt_joueurs: dict, p_categorie: str) -> list:
    """
    Retourne une liste des joueurs dans une catégorie.
    :param dt_joueurs: Base de données des joueurs.
    :param categorie: La catégorie des joueurs.
    :return: Tous les joueurs d'une catégorie.
    """
    lst_joueurs = []
    for nom, (age, categorie) in dt_joueurs.items():
        if p_categorie == categorie:
            lst_joueurs.append(nom)
    return lst_joueurs


def afficher_tout(dt_joueurs: dict) -> None:
    """
    Affiche tous les joueurs.
    :param dt_joueurs: Base de données des joueurs.
    :return: None
    """
    print(f"{Fore.YELLOW}{'Nom':<15} {'Age':^4} {'Categorie':<15}")
    for nom, (age, categorie) in dt_joueurs.items():
        print(f"{Fore.GREEN}{nom:<15} {Fore.BLUE}{age:^4} {Fore.RED}{categorie:<15}")


def afficher_menu() -> str:
    """
    Affiche le menu des choix.
    :return: Le choix de l'utilisateur.
    """
    print(f"\n1. Entrer un joueur")
    print(f"2. Afficher tous les joueurs")
    print(f"3. Afficher les joueurs par catégorie")
    print(f"4. Afficher les joueurs par age croisant ou décroissant")
    print(f"0. Quitter")
    return input(f"{Fore.YELLOW}Votre choix: ")


def categories_de_sport():
    dt_joueurs = {
        "Justin": [8, "Benjamins"],
        "Sahara": [15, "Élites"],
        "Abdel": [8, "Benjamins"]
    }
    categories = ["Mini-poussins", "Poussins", "Benjamins", "Cadets", "Élites", "Adultes"]

    # Explique le programme.
    print(f"{Fore.BLUE}Ce programme permet de classer les joueurs de sport en catégories d'âge.")

    # Boucle principale.
    while True:
        choix = afficher_menu()
        if choix == "1":  # Ajouter un joueur.
            joueur = ajouter_joueur()
            if joueur is None:
                print(f"{Fore.RED}Aucun joueur n'a été ajouté.")
            else:
                dt_joueurs.update(joueur)
                print(f"{Fore.BLUE}{joueur.keys()} a été ajouté avec succès.")
        elif choix == "2":  # Afficher tous les joueurs.
            afficher_tout(dt_joueurs)
        elif choix == "3":  # Afficher les joueurs par catégorie.
            print(f"{Fore.BLUE}\nLes catégories sont: {Fore.GREEN}{categories}")
            categorie = input(f"{Fore.YELLOW}Entrez la catégorie: ").capitalize()
            if categorie in categories:
                lst_joueurs = categorie_joueurs(dt_joueurs, categorie)
                if lst_joueurs == []:
                    print(f"{Fore.RED}Aucun joueur dans cette catégorie.")
                else:
                    print(f"{Fore.BLUE}Les joueurs cette catégorie sont: ", end="")
                    str_noms = ""
                    for joueur in lst_joueurs:
                        str_noms += f"{joueur}, "
                    print(f"{Fore.GREEN}{str_noms[:-2]}")  # On enlève la virgule et l'espace à la fin.
            else:
                print(f"{Fore.RED}Cette catégorie n'existe pas.")
        elif choix == "4":  # Afficher les joueurs par age.
            decroissant = input(f"{Fore.YELLOW}Trier par ordre décroissant? (O/N): ")
            if decroissant.upper() == "O":
                decroissant = True
            else:
                decroissant = False
            lst_joueurs = tri_age(dt_joueurs, decroissant)
            print(f"{Fore.BLUE}Les joueurs par age sont: ")
            for age, nom in lst_joueurs:
                print(f"{Fore.GREEN}{nom:15} : {Fore.BLUE}{age:4} ans")
        elif choix == "0":  # Quitter le programme.
            print(f"{Fore.BLUE}\nAu revoir!")
            sys.exit()
        else:  # Mauvais choix.
            print(f"{Fore.RED}Choix invalide.")


if __name__ == "__main__":
    categories_de_sport()
