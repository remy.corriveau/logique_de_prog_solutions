import pytest
import categories_de_sport


@pytest.mark.parametrize("age, categorie", [
    (10, "Cadets"),
    (16, "Élites"),
    (17, "Adultes")
])
def test_selectionner_categorie(age, categorie):
    assert categories_de_sport.selectionner_categorie(age) == categorie


