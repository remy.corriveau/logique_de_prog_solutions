import sys

# AFFICHER menu [cyan, magenta, jaune]
print("""
Les couleurs disponibles pour le mélange sont : 
cyan
magenta
jaune

*** Ne choisissez pas deux fois la même couleur ! ***
""")

try:
    #  LIRE les choix
    couleur1 = input("Quel est la première couleur choisie ? ").casefold()
    couleur2 = input("Quel est la deuxième couleur choisie ? ").casefold()
    if not couleur1.isalpha() or not couleur2.isalpha():
        raise TypeError
    if couleur1 not in ["cyan", "magenta", "jaune"] or couleur2 not in ["cyan", "magenta", "jaune"]:
        raise NameError
    if couleur1 == couleur2:
        raise ValueError
except TypeError:
    sys.exit("Vous devez entrer un mot.")
except NameError:
    sys.exit("Les seuls noms valides sont cyan, magenta et jaune.")
except ValueError:
    sys.exit("Vous ne pouvez pas mettre 2 fois la même couleur.")


# Mettre de l'espace dans l'affichage.
print()

# TROUVER le résultat du mélange
if (couleur1 == "cyan" and couleur2 == "magenta") \
        or (couleur1 == "magenta" and couleur2 == "cyan"):
    melange = "violet"

elif (couleur1 == "magenta" and couleur2 == "jaune") \
        or (couleur1 == "jaune" and couleur2 == "magenta"):
    melange = "orange"

else:
    melange = "vert"

# AFFICHER le résultat du mélange
print(f"Le résultat du mélange est {melange}.")
