# Variables
# prix_unite = 18.50
import sys

prix_1_mois = 95.00
prix_3_mois = 265.00
prix_6_mois = 430.00
prix_12_mois = 695.00
pourcent_taxe = 0.14975

# J'ai décidé de lire le prix unitaire au clavier
# ça me donne des plus belles exceptions.

try:
    prix_unite = float(input("Quel est le prix pour une visite ? "))
    if prix_unite == 0:  # Une condition pour lever ma propre exception.
        raise ZeroDivisionError
    if prix_unite < 0:  # Une condition pour lever ma propre exception.
        raise ValueError
except ValueError:  # ValueError va être attrapé si prix_unite n'est pas un nombre ou est un nombre négatif.
    sys.exit("Vous devez entrer un nombre réel positif, au revoir.")
except ZeroDivisionError:
    sys.exit("Vous ne pouvez pas entrer la valeur zéro, au revoir.")

# LIRE le nombre de mois d'abonnement
while True:
    nb_mois_abonnement = input("Pour combien de mois désirez-vous vous abonner [1, 3, 6, 12] ? ")
    try:
        if nb_mois_abonnement not in ["1", "3", "6", "12"]:
            raise ValueError
    except ValueError:
        print("Vous n'avez pas entré 1, 3, 6 ou 12. Réessayez.\n")
    else:
        break

if nb_mois_abonnement == "1":
    # CALCULER nb fois par semaine pour 1 mois.
    nb_fois_sem = (prix_1_mois / prix_unite) / 4.33

    # CALCULER le prix réel par visite - 2 fois semaine avec abonnement 12 mois.
    prix_reel_2_fois_sem = (prix_1_mois + (prix_1_mois * pourcent_taxe)) / (4.33 * 2)

elif nb_mois_abonnement == "3":
    # CALCULER nb fois par semaine pour 3 mois.
    nb_fois_sem = (prix_3_mois / prix_unite) / 13

    # CALCULER le prix réel par visite - 2 fois semaine avec abonnement 12 mois.
    prix_reel_2_fois_sem = (prix_3_mois + (prix_3_mois * pourcent_taxe)) / (13 * 2)

elif nb_mois_abonnement == "6":
    # CALCULER nb fois par semaine pour 6 mois.
    nb_fois_sem = (prix_6_mois / prix_unite) / 26

    # CALCULER le prix réel par visite - 2 fois semaine avec abonnement 12 mois.
    prix_reel_2_fois_sem = (prix_6_mois + (prix_6_mois * pourcent_taxe)) / (26 * 2)

else:
    # CALCULER nb fois par semaine pour 12 mois.
    nb_fois_sem = (prix_12_mois / prix_unite) / 52

    # CALCULER le prix réel par visite - 2 fois semaine avec abonnement 12 mois.
    prix_reel_2_fois_sem = (prix_12_mois + (prix_12_mois * pourcent_taxe)) / (52 * 2)

# AFFICHER nombre de fois par semaine pour être rentable
print(f"\nCombien de fois par semaine doit-il aller au gym " +
f"avec un abonnement {nb_mois_abonnement} mois ?")
print(f"Il doit y aller {nb_fois_sem:.1f} fois par semaine.")

# AFFICHER le coût réel par visite s'il y va 2 fois par semaine.
print(f"\nÀ combien est-ce que ça lui revient par visite s’il " +
f"va à l’escalade 2 fois par semaines ?")
print(f"Chaque visite lui coûte réellement : {prix_reel_2_fois_sem:.2f} $ taxes incluses.")
