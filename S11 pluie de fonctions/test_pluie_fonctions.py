import pytest
import pluie_fonctions

###############calculer hypothenuse######################
@pytest.mark.parametrize("cote_a, cote_b, cote_c", [
    (2, 2, 2.8284),
    (1000, 1000, 1414.2136),
    (0.5, 0.5, 0.7071),
    (1, 1, 1.4142)
])
def test_pass_calculer_hypothenuse(cote_a, cote_b, cote_c):
    assert pluie_fonctions.calculer_hypothenuse(cote_a, cote_b) == cote_c


@pytest.mark.parametrize("cote_a, cote_b", [
    (2, "texte"),
    ("texte", 1000),
    ("texte", "texte"),
    (True, 2)
])
def test_type_error_calculer_hypothenuse(cote_a, cote_b):
    with pytest.raises(TypeError):
        pluie_fonctions.calculer_hypothenuse(cote_a, cote_b)


@pytest.mark.parametrize("cote_a, cote_b", [
    (0, 2),
    (2, 0),
    (-1, 2),
    (2, -1),
    (-300, -500)
])
def test_value_error_calculer_hypothenuse(cote_a, cote_b):
    with pytest.raises(ValueError):
        pluie_fonctions.calculer_hypothenuse(cote_a, cote_b)
################################################################


#####################calculer statistiques#########################
@pytest.mark.parametrize("ls_nombres, dt_resultat", [
    ([1, 2, 3], {'minimum': 1, 'maximum': 3, 'moyenne': 2, 'quantite': 3, 'somme': 6.0}),
    ([-5, 0, 5], {'minimum': -5, 'maximum': 5, 'moyenne': 0, 'quantite': 3, 'somme': 0}),
    ([0], {'minimum': 0, 'maximum': 0, 'moyenne': 0, 'quantite': 1, 'somme': 0})
])
def test_pass_calculer_statistiques(ls_nombres, dt_resultat):
    assert pluie_fonctions.calculer_statistiques(ls_nombres) == dt_resultat

@pytest.mark.parametrize("ls_nombres", [
    (["texte", 2, 3]),
    ([-5, True, 5]),
    ([0, (1, 2)])
])
def test_type_error_calculer_statistiques(ls_nombres):
    with pytest.raises(TypeError):
        assert pluie_fonctions.calculer_statistiques(ls_nombres)
###########################################################################


################verifier palindrome######################################
@pytest.mark.parametrize("message, message_inverse", [
    ("kayak", True),
    ("mon nom", True),
    ("radar", True)
])
def test_pass_true_verifier_palindrome(message, message_inverse):
    assert pluie_fonctions.verifier_palindrome(message) == message_inverse

@pytest.mark.parametrize("message, message_inverse", [
    ("remy", False),
    ("bleu", False),
    ("rouge", False)
])
def test_pass_false_verifier_palindrome(message, message_inverse):
    assert pluie_fonctions.verifier_palindrome(message) == message_inverse

@pytest.mark.parametrize("message", [
    (3),
    (["a", "b", "c"]),
    (True)
])
def test_type_error_verifier_palindrome(message):
    with pytest.raises(TypeError):
        assert pluie_fonctions.verifier_palindrome(message)
###########################################################################