import math
import random
import statistics
import textwrap


def calculer_hypotenuse(cote_a: float, cote_b: float) -> float:
    """Calcule l'hypothénuse à partir de la longeur des deux autres côtés.
    Paramètres: Deux valeurs numériques positives.
    Retourne: La longueur de l'hypothénuse.
    Validation: Lève un TypeError si les côtés ne sont pas des nombres.
    Validation: Lève un ValueError si les côtés < 0."""

    # Validation type nombre et valeur positive.
    for cote in (cote_a, cote_b):
        if isinstance(cote, bool):
            raise TypeError(f"La valeur {cote} est un booléen.")
        elif not isinstance(cote, (int, float)):
            raise TypeError(f"La valeur {cote} n'est pas valide.")
        elif cote <= 0:
            raise ValueError(f"Erreur: {cote}, une longueur ne peut pas être négative.")

    # Formule de l'hypotenuse : a² + b² = c²
    hypotenuse = math.sqrt((cote_a ** 2) + (cote_b ** 2))

    # Précision de 4 décimals
    return round(hypotenuse, 4)


def calculer_statistiques(ls_nombres: list) -> dict:
    """Calcule le nombre, la somme, la moyenne, le minimum et le maximum d'une liste de nombres.
    Paramètre: Liste de nombre.
    Retourne: Un dictionnaire qui contient les statisques.
    Validation: Lève un TypeError si la liste ne contient pas que des nombres."""
    dt_stats = {
        "minimum": float,
        "maximum": float,
        "moyenne": float,
        "quantite": int,
        "somme": float
    }
    # Validation
    for nombre in ls_nombres:
        if isinstance(nombre, bool):
            raise TypeError(f"La valeur {nombre} est un booléen.")
        elif not isinstance(nombre, (int, float)):
            raise TypeError("La liste doit contenir uniquement des nombres.")

    # Minimum
    dt_stats['minimum'] = min(ls_nombres)
    # Maximum
    dt_stats['maximum'] = max(ls_nombres)
    # Moyenne
    dt_stats['moyenne'] = statistics.mean(ls_nombres)
    # Quantité
    dt_stats['quantite'] = len(ls_nombres)
    # Somme
    dt_stats['somme'] = math.fsum(ls_nombres)

    return dt_stats


def verifier_palindrome(message: str) -> bool:
    """Vérifie sur une chaîne de caractères est un palindrome.
    Paramètre: Une chaîne de caractères.
    Retourne: Un booléen qui signale si la chaîne est un palindrome.
    Validation: Lève un TypeError si le paramètre n'est pas une chaîne."""
    # Validation
    if not isinstance(message, str):
        raise TypeError(f"Votre message '{message}' n'est pas une string.")

    return message == "".join(reversed(message))


def piger_cartes(quantite: int) -> tuple:
    """Pige dans la liste, une quantité "x" de cartes uniques.
    Paramètre: Un nombre entier.
    Retourne: Un tuple qui contient les cartes pigés.
    Validation: Lève un TypeError si le paramètre n'est pas un nombre entier."""
    LISTE_CARTES = ['As de coeur', '2 de coeur', '3 de coeur', '4 de coeur',
                    '5 de coeur', '6 de coeur', '7 de coeur', '8 de coeur', '9 de coeur',
                    '10 de coeur', 'Valet de coeur', 'Dame de coeur', 'Roi de coeur',
                    'As de carreau', '2 de carreau', '3 de carreau', '4 de carreau',
                    '5 de carreau', '6 de carreau', '7 de carreau', '8 de carreau',
                    '9 de carreau', '10 de carreau', 'Valet de carreau', 'Dame de carreau',
                    'Roi de carreau', 'As de pique', '2 de pique', '3 de pique', '4 de pique',
                    '5 de pique', '6 de pique', '7 de pique', '8 de pique', '9 de pique',
                    '10 de pique', 'Valet de pique', 'Dame de pique', 'Roi de pique',
                    'As de trèfle', '2 de trèfle', '3 de trèfle', '4 de trèfle',
                    '5 de trèfle', '6 de trèfle', '7 de trèfle', '8 de trèfle',
                    '9 de trèfle', '10 de trèfle', 'Valet de trèfle', 'Dame de trèfle',
                    'Roi de trèfle']
    # Validation de la quatité.
    if not isinstance(quantite, int):
        raise TypeError("La quantite doit être un nombre entier. (int)")

    # Faire la pige.
    cartes_pige = tuple(random.sample(LISTE_CARTES, k=quantite))
    return cartes_pige


def afficher_table_multiplication():
    """Affiche la table de multiplication 10 x 10.
    Paramètre: Aucun.
    Retourne: Aucun.
    Validation: Aucune."""

    # Afficher la table de multiplication 10 x 10
    for rangee in range(1, 11):
        for colonne in range(1, 11):
            produit = rangee * colonne
            print(f"{produit:3}", end=" ")
        print()  # Changement de ligne.


def afficher_banniere(message: str, symbole: str = "*"):
    """Affiche le texte reçu, mais entouré d'une bannière.
    Fonctionne sur plusieurs lignes.
    Paramètre: Chaîne de caractères pour le message à afficher.
    Paramètre: Symbole pour construire le contour.
    Retourne: Aucun.
    Validation: Lève un TypeError si le paramètre n'est pas une chaîne."""

    # Validation du type de message.
    if not isinstance(message, str):
        raise TypeError("Le message doit être de type 'string'.")

    # Calculer la longueur de la plus longue chaîne
    ls_lignes_du_message = message.splitlines()
    longueur_max = len(max(ls_lignes_du_message))
    print(symbole * (longueur_max + 4))

    # Affichage de la bannière
    for ligne in ls_lignes_du_message:
        print(f"{symbole} {ligne} {symbole}")
    print(symbole * (longueur_max + 4))


def afficher_foobar(nb_iteration: int):
    """Affiche le texte reçu, mais entouré d'une bannière.
    Paramètre: Chaîne de caractères pour le message à afficher.
    Retourne: Aucun.
    Validation: Lève un TypeError si le paramètre n'est pas une chaîne."""

    # Validation du nombre d'itération
    if not isinstance(nb_iteration, int):
        raise TypeError("Le nombre d'itération doit être un nombre entier.")

    # Afficher la boucle FooBar
    for iteration in range(1, nb_iteration + 1):
        if iteration % 3 == 0 and iteration % 5 == 0:
            print("FooBar")
        elif iteration % 3 == 0:
            print("Foo")
        elif iteration % 5 == 0:
            print("Bar")
        else:
            print(iteration)


def afficher_menu() -> str:
    """
    Afficher le menu des fonctions possibles.
    :return: Le choix de l'utilisateur.
    """
    return input(textwrap.dedent("""
    1 - Calculer l'hypoténuse
    2 - Statistiques pour liste de nombres
    3 - Vérifier palindrome
    4 - Pige de cartes
    5 - Afficher la table de multiplication 10x10
    6 - Bannière de texte
    7 - FooBar
    
    Quel est votre choix ? """))


def main():
    str_choix_fct = afficher_menu()

    # Hypoténuse
    if str_choix_fct == "1":
        cote_a = float(input("Veuillez entrer la longeur du premier côté : "))
        cote_b = float(input("Veuillez entrer la longeur du deuxième côté : "))
        hypotenuse = calculer_hypotenuse(cote_a, cote_b)
        print(f"Triangle {cote_a:.2f}, {cote_b:.2f} : {hypotenuse:.2f}")

    # Statistiques
    elif str_choix_fct == "2":
        ls_nb = []
        while True:
            nb = float(input("Veuillez entrer un nombre : "))
            if nb == -1:
                break
            ls_nb.append(nb)
        dt_stats = calculer_statistiques(ls_nb)
        print(textwrap.dedent(f"""
        Quantité : {dt_stats['quantite']}
        Somme    : {dt_stats['somme']}
        Moyenne  : {dt_stats['moyenne']}
        Minimum  : {dt_stats['minimum']}
        Maximum  : {dt_stats['maximum']}
        """))

    # Palindrome
    elif str_choix_fct == "3":
        mot = input("Veuillez entre le mot à vérifier : ")
        print(f"{mot} : {verifier_palindrome(mot)}")

    # Piger des cartes
    elif str_choix_fct == "4":
        qt_cartes = int(input("Combien de cartes voulez-vous piger ? "))
        tp_cartes = piger_cartes(qt_cartes)
        print(tp_cartes)

    # Table de multiplication 10x10
    elif str_choix_fct == "5":
        afficher_table_multiplication()

    elif str_choix_fct == "6":
        texte = input("Entrez le texte pour la bannière : ")
        contour = input("Entrez le contour pour la bannière : ")
        afficher_banniere(symbole=contour, message=texte)

    elif str_choix_fct == "7":
        nb_lignes = int(input("Combien de lignes pour FooBar ? "))
        afficher_foobar(nb_lignes)

    else:
        print("Votre sélection n'est pas valide. Au revoir.")


if __name__ == "__main__":
    main()
