def valeur_num_lettre(lettre: str) -> int:
    """
    Détermine la valeur numérique d'une lettre.
    Il n'y a pas de distinction selon la casse.
    :param lettre: La lettre à convertir
    :return: La valeur numérique de la lettre
    """
    # La position + 1 détermine la valeur numérique
    alphabet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k","l", "m",
                "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]

    return alphabet.index(lettre.lower()) + 1


def valeur_num_mot_bonus(mot: str) -> int:
    """
    Détermine la valeur numérique d'un mot.
    Il n'y a pas de distinction selon la casse.
    :param mot: Le mot à convertir
    :return: La valeur numérique du mot
    """
    # La position + 1 détermine la valeur numérique
    alphabet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k","l", "m",
                "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]

    total = 0
    for lettre in mot:
        total += alphabet.index(lettre.lower()) + 1

    return total

