def est_pair(nombre: int) -> bool:
    """
    Détermine si un nombre est pair
    :param nombre: Nombre à vérifier
    :return: True si le nombre est pair, False sinon
    """
    return nombre % 2 == 0







