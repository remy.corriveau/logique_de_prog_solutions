import nombre_pair
import taxe
import casse
import valeur_lettre

# On utilise les modules à partir d'ici

# nombre_pair
nombre_a_verifier = int(input("Entrez un nombre: "))
if nombre_pair.est_pair(nombre_a_verifier):
    print(f"Le nombre {nombre_a_verifier} est pair.")
else:
    print(f"Le nombre {nombre_a_verifier} est impair.")

# taxe
montant = float(input("\nEntrez un montant auquel seront calculés les taxes : "))
tps, tvq, total = taxe.calculer_taxes(montant)
print(f"""
Montant avant taxes : {montant:8.2f} $
                TPS : {tps:8.2f} $
                TVQ : {tvq:8.2f} $
                      __________
              Total : {total:8.2f} $
""")

# casse
lettre = input("\nEntrez une lettre à inverser: ")
print(f"La lettre inversée est {casse.inverser_casse(lettre)}")
mot = input("\nEntrez un mot à inverser: ")
print(f"Le mot inversé est {casse.inverser_casse_bonus(mot)}")

# valeur_lettre
lettre = input("\nEntrez une lettre pour connaître sa valeur numérique: ")
print(f"La valeur numérique de la lettre '{lettre}' est {valeur_lettre.valeur_num_lettre(lettre)}")
mot = input("\nEntrez un mot pour connaître sa valeur numérique: ")
print(f"La valeur numérique du mot '{mot}' est {valeur_lettre.valeur_num_mot_bonus(mot)}")




