def inverser_casse(lettre: str) -> str:
    """
    Inverse la casse d'une lettre.
    :param lettre: La lettre à inverser.
    :return: La lettre inversée.
    """
    if lettre.isupper():
        return lettre.lower()
    else:
        return lettre.upper()

def inverser_casse_bonus(mot: str) -> str:
    """
    Inverse la casse d'une mot.
    :param mot: Le mot à inverser.
    :return: Le mot inversée.
    """
    return mot.swapcase()


