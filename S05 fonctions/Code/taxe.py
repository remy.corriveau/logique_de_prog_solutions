def calculer_taxes(montant_avant_taxe: float) -> tuple:
    """
    Calculer les taxes de vente pour un montant donné.
    :param montant_avant_taxe: Montant avant taxe
    :return: taxe fédérale, taxe provinciale, total des taxes
    """
    TAUX_TPS = 0.05
    TAUX_TVQ = 0.09975
    montant_tps = montant_avant_taxe * TAUX_TPS
    montant_tvq = montant_avant_taxe * TAUX_TVQ
    montant_total = montant_avant_taxe + montant_tps + montant_tvq
    return montant_tps, montant_tvq, montant_total

