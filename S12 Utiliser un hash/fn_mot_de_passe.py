from getpass import getpass
import hashlib
import secrets
from textwrap import dedent
from datetime import datetime, timedelta
from colorama import Fore, init

init(autoreset=True)


def generer_sel() -> str:
    """
    Générer un sel de 16 caractères aléatoires.
    :return: Le sel généré
    """
    return secrets.token_hex(16)


def hachage(mot_de_passe: str, sel: str) -> str:
    """
    Hacher le mot de passe avec l'algorithme SHA-256.
    :param mot_de_passe: Le mot de passe à hacher
    :param sel: Le sel à utiliser
    :return: Le mot de passe haché
    """
    mot_de_passe_avec_sel = mot_de_passe + sel
    return hashlib.sha256(mot_de_passe_avec_sel.encode()).hexdigest()


def verifier_mot_de_passe(mot_de_passe: str, derniere_mod: str = None) -> bool:
    """
    Vérifier si le mot de passe respecte les critères de sécurité.
    Longeur de 6 caractères, avec au moins une majuscule, une minuscule et un chiffre.
    :param mot_de_passe: Le mot de passe à vérifier
    :param derniere_mod: La date de dernière modification du mot de passe, optionnel
    :return: True si le mot de passe est valide, False sinon
    """
    longueur, majuscule, minuscule, chiffre, chiffre_debut, interval = False, False, False, False, False, False

    if len(mot_de_passe) >= 6:
        longueur = True

    if not mot_de_passe[0].isdigit():
        chiffre_debut = True

    if derniere_mod:
        interval = datetime.now() - datetime.strptime(derniere_mod, "%Y-%m-%d %H:%M:%S") > timedelta(minutes=5)
    else:
        interval = True

    for char in mot_de_passe:
        if char.isupper():
            majuscule = True
        elif char.islower():
            minuscule = True
        elif char.isdigit():
            chiffre = True

    if not longueur:
        print(f"{Fore.RED}Le mot de passe doit contenir au moins 6 caractères.")
    if not chiffre_debut:
        print(f"{Fore.RED}Le mot de passe doit commencer par un chiffre.")
    if not interval:
        print(f"{Fore.RED}Le mot de passe a été changé il y a moins de 5 minutes, veuillez attendre.")
    if not majuscule:
        print(f"{Fore.RED}Le mot de passe doit contenir au moins une majuscule.")
    if not minuscule:
        print(f"{Fore.RED}Le mot de passe doit contenir au moins une minuscule.")
    if not chiffre:
        print(f"{Fore.RED}Le mot de passe doit contenir au moins un chiffre.")

    return longueur and majuscule and minuscule and chiffre and chiffre_debut and interval


def ajouter_employe(dt_employes: dict) -> dict:
    """
    Créer un nouvel employé, avec un mot de passe valide.
    :return: Le nom d'usager et les infos de mot de passe de l'employé
    """
    # Vérifier si l'employé existe déjà
    nom_utilisateur = input("Entrez le nom de l'employé : ")
    if nom_utilisateur in dt_employes:
        print(f"{Fore.RED}L'employé existe déjà.")
        return dt_employes
    elif nom_utilisateur == "":
        print(f"{Fore.RED}Une chaîne vide n'est pas un nom d'employé valide.")
        return dt_employes

    # Vérifier si le mot de passe est valide
    mot_de_passe = getpass("Entrez le mot de passe de l'employé : ")
    while not verifier_mot_de_passe(mot_de_passe):
        mot_de_passe = getpass("\nEntrez le mot de passe de l'employé : ")

    # Hacher le mot de passe
    mot_de_passe = hachage(mot_de_passe, sel := generer_sel())

    # Ajouter le nouvel employé à la base de données
    nouvel_employe = {
        nom_utilisateur: {"mdp": mot_de_passe, "sel": sel,
                          "derniere_mod": datetime.now().strftime("%Y-%m-%d %H:%M:%S")}}
    dt_employes.update(nouvel_employe)
    print(f"{Fore.GREEN}Employé ajouté avec succès !")
    return dt_employes


def modifier_mot_de_passe(dt_employes: dict) -> dict:
    """
    Modifier le mot de passe d'un employé existant.
    :param dt_employes: La base de données des employés
    :return: La base de données des employés, avec le mot de passe modifié
    """
    # Vérifier si l'employé existe
    nom_utilisateur = input("Entrez le nom de l'employé : ")
    if nom_utilisateur not in dt_employes:
        print(f"{Fore.RED}L'employé n'existe pas.")
        return dt_employes

    # Vérifier si l'ancien mot de passe est valide
    ancien_mot_de_passe = getpass("Entrez l'ancien mot de passe de l'employé : ")
    if hachage(ancien_mot_de_passe, dt_employes[nom_utilisateur]["sel"]) != dt_employes[nom_utilisateur]["mdp"]:
        print(f"{Fore.RED}Le mot de passe est incorrect.")
        return dt_employes

    # Demander le nouveau mot de passe
    mot_de_passe = getpass("Entrez le nouveau mot de passe de l'employé : ")
    # Vérifier si le nouveau mot de passe est valide
    while not verifier_mot_de_passe(mot_de_passe, dt_employes[nom_utilisateur]["derniere_mod"]):
        mot_de_passe = getpass("Entrez le nouveau mot de passe de l'employé : ")

    # Hacher le mot de passe
    mot_de_passe = hachage(mot_de_passe, dt_employes[nom_utilisateur]["sel"])

    # Modifier le mot de passe de l'employé
    dt_employes[nom_utilisateur]["mdp"] = mot_de_passe
    dt_employes[nom_utilisateur]["derniere_mod"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    print(f"{Fore.GREEN}Mot de passe modifié avec succès !")
    return dt_employes


def afficher_employes(dt_employes: dict):
    """
    Afficher la base de données des employés.
    :param dt_employes: La base de données des employés
    """
    print(f'\n{Fore.BLUE}{"Utilisateur":^20}{"Sel":^34}{"Mot de passe":^70}{"Dernière modification":<20}')
    for nom, infos in dt_employes.items():
        print(f"{nom:<20}{infos['sel']:<34}{infos['mdp']:<70}{infos['derniere_mod']:<20}")
