import pytest
import fn_mot_de_passe


@pytest.mark.parametrize("mdp, resultat_attendu", [
    ("Password1", True),
    ("1Password", False),
    ("a", False),
    ("AbCd3", False),
    ("AbCd3e", True),
    ("password1", False),
    ("PASSWORD1", False),
    ("UnTrucAvecDesChiffres123", True)
])
def test_verifier_mot_de_passe(mdp, resultat_attendu):
    assert fn_mot_de_passe.verifier_mot_de_passe(mdp) == resultat_attendu
