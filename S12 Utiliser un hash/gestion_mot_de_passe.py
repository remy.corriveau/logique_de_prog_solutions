import fn_mot_de_passe
from textwrap import dedent
import sys
from colorama import Fore, init
init(autoreset=True)


def gestion_mot_de_passe():
    dt_employes = {
        "Dupont": {"mdp": "1e0554fbce7c36c1b799eee6117d2a208ef66ca36fd8d6cd4f36a7f27416d4ec",
                   "sel": "a4ffffd421e000a0337e025a667fc4c5",
                   "derniere_mod": "2021-10-01 12:00:00"},
        "Dupond": {"mdp": "19513fdc9da4fb72a4a05eb66917548d3c90ff94d5419e1f2363eea89dfee1dd",
                   "sel": "c5af4dcc9e01af77df3cd66197ba9b11",
                   "derniere_mod": "2021-10-01 12:00:00"},
        "Dupuis": {"mdp": "62601ccb309bb5506a8265873c3b6629b7120924c7fe8fd3f36924213229f803",
                     "sel": "0de40670b3115497de5c3fb1f0f2e67d",
                   "derniere_mod": "2021-10-01 12:00:00"}}

    print(f"{Fore.BLUE}** Bienvenue dans le gestionnaire de mot de passe **")

    while True:
        print(dedent("""
        1.	Ajouter un employé (avec mot de passe)
        2.	Modifier le mot de passe d’un employé
        3.	Afficher la base de données des employés (avec mots de passe)
        4.	Quitter l’application
        """))
        input_utilisateur = input("Entrez votre choix : ")

        match input_utilisateur:
            case "1":
                dt_employes = fn_mot_de_passe.ajouter_employe(dt_employes)
            case "2":
                dt_employes = fn_mot_de_passe.modifier_mot_de_passe(dt_employes)
            case "3":
                fn_mot_de_passe.afficher_employes(dt_employes)
            case "4":
                print("Merci d'avoir utilisé le gestionnaire de mot de passe.")
                print(f"{Fore.BLUE}Bonne journée !")
                sys.exit()
            case _:
                print(f"{Fore.RED}Choix invalide, vous devez entrer un chiffre entre 1 et 4.")


if __name__ == '__main__':
    gestion_mot_de_passe()
