# Déclaration des variables
int1 = 4  # Type integer
int2 = 60  # Type integer
flt1 = 12.5  # Type float
flt2 = 3.3  # Type float
str1 = "bas"  # Type string
str2 = "moyen"  # Type string
str3 = "haut"  # Type string

# Section int et float
# Calculer la somme de int1 et int2
somme = None

# Afficher la somme
print(f"La somme de {int1} et {int2} est : {somme}")

# Calculer la différence de flt1 et flt2
difference = None

# Afficher la différence
print(f"La différence de {flt1} et {flt2} est : {difference}")

# Calculer la différence de int2 et int1 au carré
difference_avec_puissance = None

# Afficher la différence
print(f"La différence de {int2} et {int1} au carré est : {difference_avec_puissance}")

# Calculer la somme de int1 + flt2 puis multipliez par 2
priorite = None

# Afficher la différence
print(f"La somme de {int1} et {flt2} ensuite multiplié par 2 est : {priorite}")

# Section string
# À l'aide d'opérateur(s) créez la chaîne de caractères en commentaire

# basbasbas
chaine1 = None
print(f"La chaîne 1 est : {chaine1}")

# basmoyenhaut
chaine2 = None
print(f"La chaîne 2 est : {chaine2}")

# bas moyen haut, utilisez une chaîne vide pour les espaces
chaine3 = None
print(f"La chaîne 3 est : {chaine3}")
