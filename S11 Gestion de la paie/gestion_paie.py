import sys
from textwrap import dedent
from colorama import Fore, init
init(autoreset=True)


def afficher_menu() -> str:
    """
    Affiche le menu de gestion de la paie
    :return: Le choix de l'utilisateur
    """
    print(dedent("""
1.	Ajouter employé.e.s
2.	Calculer salaire(s)
3.	Afficher les informations des employé.e.s
0.	Quitter le programme
    """))
    return input("Quel est votre choix : ")


def creer_employe() -> list or None:
    """
    Créer une liste contenant les informations d'un employé
    :return: La liste avec le nouvel employé
    """
    def return_employe(nouvel_employe=None) -> list or None:
        if nouvel_employe is None:
            print(f"{Fore.RED}Aucun employé n'a été ajouté.")
            return None
        else:
            print(f"{Fore.GREEN}L'employé {Fore.YELLOW}{nouvel_employe}{Fore.GREEN} a été ajouté.")
            return nouvel_employe

    nom = input("Nom de l'employé.e : ")
    if nom == "":
        return return_employe()

    # Le statut de l’employé.e peut être égal soit à « P » pour permanent soit à « C » pour contractuel.
    statut = input("Type d'employé.e (P/C) : ").upper()
    if statut == "":
        return return_employe()
    while statut not in ["P", "C"]:
        print(f"{Fore.RED}Le type d'employé.e doit être P ou C.\n")
        statut = input("Type d'employé.e (P/C) : ").upper()

    # L’ancienneté de l’employé.e est un nombre réel compris entre 0.1 et 45.
    while True:
        anciennete = input("Ancienneté : ")
        if anciennete == "":
            return return_employe()
        try:
            anciennete = float(anciennete)
            if anciennete < 0.1 or anciennete > 45:
                raise ValueError
        except ValueError:
            print(f"{Fore.RED}L'ancienneté doit être un nombre réel entre 0.1 et 45.\n")
        else:
            break
    return return_employe([nom, statut, anciennete])


def calculer_salaires(employes: list) -> list:
    """
    Calcule les salaires des employés
    :param employes: La liste des employés
    :return: La liste des salaires
    """
    SALAIRE_BASE = 50000
    salaires = []
    for nom, statut, anciennete in employes:
        if statut == "C":
            salaire = SALAIRE_BASE + 1000 * anciennete
        elif statut == "P":
            if anciennete < 10:
                salaire = SALAIRE_BASE + 1000 * anciennete
            else:
                salaire = SALAIRE_BASE + 1500 * anciennete
        else:  # Statut invalide
            salaire = SALAIRE_BASE
        salaires.append(salaire)
    print(f"{Fore.GREEN}La list de salaires :{Fore.YELLOW} {salaires}")  # On affiche la liste des salaires
    return salaires


def afficher_employes(employes: list, salaires: list) -> None:
    """
    Affiche les informations des employés
    :param employes: La liste des employés
    :param salaires: La liste des salaires
    :return: Rien.
    """
    # Calculer la liste des salaires, s'ils n'ont pas été calculés
    if len(salaires) == 0:
        salaires = calculer_salaires(employes)

    # Afficher les informations des employés
    for i, (nom, statut, anciennete) in enumerate(employes):
        print(f"**************************************")
        print(f"Nom et prénom : {nom}")
        if statut == "C":
            print(f"Statut : Contractuel")
        elif statut == "P":
            print(f"Statut : Permanent")
        else:
            print(f"Statut : Invalide")
        print(f"Ancienneté : {anciennete}")
        print(f"Salaire : {salaires[i]:.2f} $")
        print(f"**************************************")


def quitter() -> None:
    """
    Affiche un message de remerciement et quitte le programme
    :return: Rien.
    """
    print(f"{Fore.GREEN}\nMerci d'avoir utilisé le programme de gestion de la paie.")
    sys.exit()


def gestion_paie():
    # Liste avec les infos de tous les employés
    employes = [["Jean Dupont", "C", 1.3], ["Marie Dupond", "P", 11.0], ["Pierre Dupuis", "P", 30]]
    salaires = []  # Les salaires ne sont pas encore calculés

    while True:
        choix = afficher_menu()

        match choix:
            case "1":
                nouvel_employe = creer_employe()
                if nouvel_employe is not None:  # S'il y a un nouvel employé, on l'ajoute à la liste
                    employes.append(creer_employe())
            case "2":
                salaires = calculer_salaires(employes)
            case "3":
                afficher_employes(employes, salaires)
            case "0":
                quitter()
            case _:
                print(f"{Fore.RED}Choix invalide, veuillez entrez 1, 2, 3 ou 0.")


if __name__ == "__main__":
    gestion_paie()
