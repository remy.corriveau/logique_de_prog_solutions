import pytest
import chiffrement_sym


@pytest.mark.parametrize("message_chiffre, cle, message_dechiffre", [
    ("gAAAAABlVr9nIpjJynfKJSbgeEbBM1ZWGhWls-0JxYkHOlAbj_qtcO43WP81rbd1pnVCtBhGyeMic--FPEjv7VWyfgoLFarlSg==",
     "2qfqiXNzfdDlu8GHOmK9osqq32gX3SKVANM3ibKyYuY=", "Allo"),
    ("gAAAAABlVr_1eyILrV52fJPWrRJx7_S9X6jydrm-ZH5F6i-xqbe6pmSoNf_Zyzr-87x3thJp5dITpzKCg-Ol7itWvy8fS_Cma4iwmmjJQTDu3iuVDlpfcxY=",
     "mZGpJqGeP6UWAc9-NBjhgQbV0kjzJpow1-t0-wd-h80=", "Chut ! C'est un secret.")
])
def test_dechiffrer_message(message_chiffre, cle, message_dechiffre):
    assert chiffrement_sym.dechiffrer_message(message_chiffre, cle) == message_dechiffre


def test_chiffrement_sym():
    message = "Le chocolat, c'est la meilleure chose !"
    cle = chiffrement_sym.generer_cle()
    message_chiffre = chiffrement_sym.chiffrer_message(message.encode(), cle)
    message_dechiffre = chiffrement_sym.dechiffrer_message(message_chiffre, cle)
    assert message_dechiffre == message