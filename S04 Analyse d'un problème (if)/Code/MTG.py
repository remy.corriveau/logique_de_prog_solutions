# Déclaration des variables
PRIX_COMMON = 0.05
PRIX_UNCOMMON = 0.25
PRIX_RARE = 3
PRIX_MYTHIC = 10
QT_COMMON = 1428
QT_UNCOMMON = 429
QT_RARE = 125
QT_MYTHIC = 18
BONUS_UNITAIRE = 1.5
LS_NOM_CAT = ("common", "uncommon", "rare", "mythic")

# Calculer le montant de vente pour chaque catégorie.
ls_total_par_cat = [PRIX_COMMON * QT_COMMON, PRIX_UNCOMMON * QT_UNCOMMON, PRIX_RARE * QT_RARE, PRIX_MYTHIC * QT_MYTHIC]
# total_common = PRIX_COMMON * QT_COMMON
# total_uncommon = PRIX_UNCOMMON * QT_UNCOMMON
# total_rare = PRIX_RARE * QT_RARE
# total_mythic = PRIX_MYTHIC * QT_MYTHIC

# Calculer la catégorie qui vaut le plus cher et lui ajouter le bonus pour la vente unitaire.
montant_cat_plus_payante = max(ls_total_par_cat)
total_cat_bonus = montant_cat_plus_payante * BONUS_UNITAIRE

# Trouver le nom de la catégorie la plus payante
position_cat_plus_payante = ls_total_par_cat.index(montant_cat_plus_payante)
nom_cat_plus_payante = LS_NOM_CAT[position_cat_plus_payante]

# Calculer le montant total de vente de toutes les catégories, incluant le bonus pour la plus chère.
ls_total_par_cat.remove(montant_cat_plus_payante)
ls_total_par_cat.append(total_cat_bonus)
total_vente = sum(ls_total_par_cat)

# Afficher la catégorie la plus payante.
print(f"""
La catégorie la plus payante à vendre à l'unité est : {nom_cat_plus_payante}
et sa valeur est de {montant_cat_plus_payante:.2f}$""")

# Affiche le montant total des ventes.
print(f"\nLe total des ventes est de {total_vente:.2f}$")
