import math

# déclaration des variables
NB_JUDOKAS = 13
NB_ENTRAINEUR = 1
PRIX_CHAMBRE = 79
PRIX_TRANSPORT = 223
PRIX_BROSSE_DENTS = 4
TAUX_PROFIT_BROSSE = 1 / 2
CONTRIBUTION_PARENT = 20

# Calculer le nombre de chambres nécessaires en occupation double et quadruple.
# if NB_JUDOKAS % 2 != 0:
#     nb_chambre_double = NB_JUDOKAS // 2 + 1 + NB_ENTRAINEUR
# else:
#     nb_chambre_double = NB_JUDOKAS / 2 + NB_ENTRAINEUR

nb_chambre_double = math.ceil(NB_JUDOKAS / 2) + NB_ENTRAINEUR
nb_chambre_quadruple = math.ceil(NB_JUDOKAS / 4) + NB_ENTRAINEUR

# Calculer le profit à la vente d’une brosse à dents.
profit_par_brosse = PRIX_BROSSE_DENTS * TAUX_PROFIT_BROSSE

# Calculer le total à débourser pour les chambres et le transport en occupation double et quadruple.
montant_total_double = PRIX_TRANSPORT + (nb_chambre_double * PRIX_CHAMBRE)
montant_total_quadruple = PRIX_TRANSPORT + (nb_chambre_quadruple * PRIX_CHAMBRE)

# Calculer le total moins la contribution des parents.
montant_total_double_parents = montant_total_double - (NB_JUDOKAS * CONTRIBUTION_PARENT)
montant_total_quadruple_parents = montant_total_quadruple - (NB_JUDOKAS * CONTRIBUTION_PARENT)

# Calculer le nombre de brosses à dents à vendre pour couvrir les frais pour l’équipe et individuellement par judoka. Avec et sans et contribution des parents.
qt_brosse_double = math.ceil(montant_total_double / profit_par_brosse)
qt_brosse_quadruple = math.ceil(montant_total_quadruple / profit_par_brosse)
qt_brosse_double_parents = math.ceil(montant_total_double_parents / profit_par_brosse)
qt_brosse_quadruple_parents = math.ceil(montant_total_quadruple_parents / profit_par_brosse)

qt_brosse_double_par_judoka = math.ceil(qt_brosse_double / NB_JUDOKAS)
qt_brosse_quadruple_par_judoka = math.ceil(qt_brosse_quadruple / NB_JUDOKAS)
qt_brosse_double_parents_par_judoka = math.ceil(qt_brosse_double_parents / NB_JUDOKAS)
qt_brosse_quadruple_parents_par_judoka = math.ceil(qt_brosse_quadruple_parents / NB_JUDOKAS)

# Afficher les montants pour toutes les situations
print(f"""
La quantité de brosses à dents nécessaire pour couvrir les frais.
                                    POUR LE GROUPE INDIVIDUELLEMENT
Occupation double                 : {qt_brosse_double:^14} {qt_brosse_double_par_judoka:^16}
Occupation quadruple              : {qt_brosse_quadruple:^14} {qt_brosse_quadruple_par_judoka:^16}
Occupation double avec parents    : {qt_brosse_double_parents:^14} {qt_brosse_double_parents_par_judoka:^16}
Occupation quadruple avec parents : {qt_brosse_quadruple_parents:^14} {qt_brosse_quadruple_parents_par_judoka:^16}

 
""")

