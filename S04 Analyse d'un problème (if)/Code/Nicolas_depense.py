# Déclaration de variable
GYM_MOIS = 60
PRODUIT_SEM = 15
COIFFEUR_3_SEM = 25
NUTRITONNISTE_6_mois = 150
MANUCURE_AN = 100

# Calculer le prix de chaque dépense en moyenne par jour.
gym_jour = GYM_MOIS * 12 / 365.25
produit_jour = PRODUIT_SEM / 7  # * 52 / 365.25
coiffeur_jour = COIFFEUR_3_SEM / 21  # * 52 / 3 / 365.25
nutritionniste_jour = NUTRITONNISTE_6_mois * 2 / 365.25
manucure_jour = MANUCURE_AN / 365.25

# Calculer le total de chaque prix par jour.
total_jour = gym_jour + produit_jour + coiffeur_jour + nutritionniste_jour + manucure_jour

# Afficher le total de chaque prix par jour.
print(f"Nicolas dépense en moyenne {total_jour:.2f}$ par jour.")
