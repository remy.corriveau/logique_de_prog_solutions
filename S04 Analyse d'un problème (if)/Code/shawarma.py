# Déclaration de variables
LOYER = 3000
CHAUFFAGE = 400
ASSURANCE = 200
ELECTRICITE = 500
SALAIRES_EMPLOYES = 9600
SALAIRE_NICO_HEBDO = 800
MARGE_PROFIT = 0.40

# Calculer le salaire à Nicolas pour un mois
salaire_nico_mois = SALAIRE_NICO_HEBDO * 52 / 12

# Calculer les dépenses totales (loyer, salaires, etc)
depenses_total = LOYER + CHAUFFAGE + ASSURANCE + ELECTRICITE + SALAIRES_EMPLOYES + salaire_nico_mois

# Calculer le montant total de nourriture à vendre
montant_a_vendre = depenses_total / MARGE_PROFIT

# Afficher le montant de nourriture à vendre
print(f"Nicolas doit vendre pour {montant_a_vendre:.2f}$ de nourriture par mois.")
