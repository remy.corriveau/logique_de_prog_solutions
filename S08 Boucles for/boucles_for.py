import textwrap


def menu_grandeurs(ls_grandeurs: list):
    """
    Affiche un menu qui offre une variété de grandeurs différentes.
    :param ls_grandeurs: Toutes les grandeurs disponibles.
    """
    print("Bienvenu au menu des grandeurs : ")
    for grandeur in ls_grandeurs:
        print(grandeur)


def afficher_questionnaire(p_nb_questions: int, p_nb_choix: int):
    """
    Affiche le gabarit pour la quantité de questions et choix demandés.
    :param p_nb_questions: Le nombre de questions.
    :param p_nb_choix: Le nombre de choix par question.
    :return: Rien. Affichage seulement.
    """
    for question in range(1, p_nb_questions + 1):
        print(f"\nQuestion {question} :")
        for choix in range(1, p_nb_choix + 1):
            print(f"- Choix {choix}")


def main():
    # Afficher le choix des fonctions
    choix = input(textwrap.dedent("""
    Choississez une option :
    1. Menu des grandeurs
    2. Afficher un questionnaire
    
    Quel est votre choix ? """))
    print()

    # Exécuter la fonction choisie
    match choix:
        case "1":
            ls_grandeurs = ["petit", "moyen", "grand"]
            menu_grandeurs(ls_grandeurs)

        case "2":
            nb_questions = int(input("Combien de questions dans le questionnaire ? "))
            nb_choix = int(input("Combien de choix par question ? "))
            afficher_questionnaire(nb_questions, nb_choix)


if __name__ == '__main__':
    main()
