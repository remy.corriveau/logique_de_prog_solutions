import pytest
import main


@pytest.mark.parametrize("prenom", [
    "Rémy",
    "Stéphane",
    "Marilou"
])
def test_pass_salutation(prenom):
    assert main.salutation(prenom) == f"Bonjour {prenom} !"


@pytest.mark.parametrize("prenom", [
    "Rémy1",
    "$téphane",
    "Mari-lou"
])
def test_type_error_salutation(prenom):
    with pytest.raises(TypeError):
        main.salutation(prenom)


def test_pass_pile_ou_face():
    assert main.pile_ou_face() in ("pile", "face")


@pytest.mark.parametrize("prenom, expected", [
    ("Rémy", "Ymér"),
    ("Stéphane", "Enahpéts"),
    ("Marilou", "Uoliram")
])
def test_pass_inverser_lettres(prenom, expected):
    assert main.inverser_lettres(prenom) == expected


@pytest.mark.parametrize("prenom, expected", [
    ("Rémy", "RéMy"),
    ("Stéphane", "StÉpHaNe"),
    ("Marilou", "MaRiLoU")
])
def test_pass_casse_alternance(prenom, expected):
    assert main.casse_alternance(prenom) == expected
