import random
import sys


def salutation(prenom: str) -> str:
    """
    Affiche une phrase de salutation personnalisée.

    :param prenom: Un prénom.
    :return: Bonjour <prenom> !
    :raise TypeError: Si prénom contient autre chose que des lettres."""
    if not prenom.isalpha():
        raise TypeError("Seulement des lettres sont acceptées dans le prénom.")
    return f"Bonjour {prenom} !"


def pile_ou_face() -> str:
    """
    Choisit pile ou face de façon aléatoire.

    :return: pile ou face
    """
    return random.choice(("pile", "face"))


def inverser_lettres(prenom: str) -> str:
    """
    Inverse l'ordre des lettres dans le prénom.
    Exemple: Rémy -> Ymér

    :param prenom: Un prénom.
    :return: Le prénom avec ses lettres inversées.
    """
    return "".join(reversed(prenom)).lower().capitalize()


def casse_alternance(prenom: str) -> str:
    """
    Alterne la casse des lettres dans le prénom.
    Exemple: Rémy -> RéMy

    :param prenom: Un prénom.
    :return: Le prénom avec alternance de la casse.
    """
    # On met tout en minuscule pour faciliter le traitement.
    prenom = prenom.lower()
    prenom_modifie = ""
    for position, lettre in enumerate(prenom):
        # Positions paires sont en majuscule.
        if position % 2 == 0:
            lettre = lettre.upper()
        prenom_modifie += lettre
    return prenom_modifie


def main():
    """
    Demande le prénom, exécute une fonction aléatoire et le modifie selon 2 possibilités.
    """

    prenom = input("Allo, nous avons besoin de votre prénom pour poursuivre, quel est-il ? ")
    try:
        print(salutation(prenom))
    except TypeError as e:
        sys.exit(e)

    # Choix aléatoire pour déterminer le traitement à faire au prénom.
    if pile_ou_face() == "pile":
        prenom_modifie = inverser_lettres(prenom)
    else:
        prenom_modifie = casse_alternance(prenom)

    print(f"\nVoici le résultat du traitement : {prenom_modifie}")


if __name__ == '__main__':
    main()
