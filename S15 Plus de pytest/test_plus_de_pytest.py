import pytest
import plus_de_pytest


# type_de_valeur
@pytest.mark.parametrize("chaine, resultat_attendu", [
    ("123", "int"),
    ("3.14", "float"),
    ("True", "bool"),
    ("False", "bool"),
    ("hello", "str"),
    ("-45", "int"),
    ("2.0", "float"),
    ("0", "int"),
    ("-12.12", "float"),
    ("abc", "str"),
])
def test_type_de_valeur(chaine, resultat_attendu):
    resultat_obtenu = plus_de_pytest.type_de_valeur(chaine)
    assert resultat_obtenu == resultat_attendu


# Nombre premier le plus proche
@pytest.mark.parametrize("nombre_entree, resultat_attendu", [
    (1, 2),
    (5, 5),
    (10, 11),
    (15, 13),
    (20, 19),
    (25, 23),
    (30, 29),
    (35, 37),
    (40, 41),
    (45, 43),
])
def test_nombre_premier_plus_proche(nombre_entree, resultat_attendu):
    resultat_obtenu = plus_de_pytest.nombre_premier_plus_proche(nombre_entree)
    assert resultat_obtenu == resultat_attendu


# Transformer mot
@pytest.mark.parametrize("mot_entree, resultat_attendu", [
    ("python", "py**on"),
    ("hello", "he*lo"),
    ("world", "wo*ld"),
    ("test", "test"),
    ("programming", "pr****ng"),
    ("abcdefgh", "ab****gh"),
    ("short", "sh*rt"),
    ("verylongword", "ve****rd"),
    ("a", "a"),
    ("", "")
])
def test_transformer_mot(mot_entree, resultat_attendu):
    resultat_obtenu = plus_de_pytest.transformer_mot(mot_entree)
    assert resultat_obtenu == resultat_attendu
