from colorama import Fore, init
import textwrap
init(autoreset=True)


def transformer_mot(mot: str) -> str:
    """
    Transformer le mot passé en paramètre en remplaçant les lettres du milieu par des étoiles.
    Le mot doit avoir au moins 5 caractères pour être transformé.
    Les mots plus longs que 8 caractères seront tronqués à 8 caractères.
    :param mot: Le mot à transformer.
    :return: Le mot transformé.
    """
    if len(mot) <= 4:  # Assure une longueur minimale de 5 caractères
        return mot
    else:
        debut = mot[:2]
        fin = mot[-2:]

        if len(mot) > 8:  # Assure une longueur maximale de 6 caractères
            milieu = "****"
        else:
            milieu = "*" * (len(mot) - 4)
        mot_transforme = debut + milieu + fin
        return mot_transforme


def est_nombre_premier(nombre: int) -> bool:
    """
    Déterminer si le nombre passé en paramètre est un nombre premier.
    :param nombre: Le nombre à vérifier.
    :return: True si le nombre est premier, False sinon.
    """
    if nombre < 2:
        return False
    for i in range(2, int(nombre**0.5) + 1):
        if nombre % i == 0:
            return False
    return True


def nombre_premier_plus_proche(nombre: int) -> int:
    """
    Trouver le nombre premier le plus proche du nombre passé en paramètre.
    :param nombre: Le nombre à vérifier.
    :return: Le nombre premier le plus proche, si deux nombres sont à égal distance, le plus petit est retourné.
    """
    if nombre < 2:
        return 2  # Le premier nombre premier est 2

    if est_nombre_premier(nombre):
        return nombre  # Le nombre est lui-même premier

    nombre_inf = nombre - 1
    nombre_sup = nombre + 1

    while True:
        if est_nombre_premier(nombre_inf):
            return nombre_inf
        elif est_nombre_premier(nombre_sup):
            return nombre_sup
        else:
            nombre_inf -= 1
            nombre_sup += 1


def type_de_valeur(chaine: str) -> str:
    """
    Déterminer le type de la valeur passée en paramètre.
    :param chaine: La valeur à analyser.
    :return: Le type de la valeur.
    """
    try:
        # Essayez de convertir la chaîne en float
        float_value = float(chaine)
        # Si la conversion en float réussit, vérifiez si elle peut être également convertie en int
        if float_value.is_integer() and chaine.find(".") == -1:
            return "int"
        else:
            return "float"
    except ValueError:
        # Si la conversion en float échoue, vérifiez si la valeur est True ou False
        if chaine.lower() == "true" or chaine.lower() == "false":
            return "bool"
        else:
            return "str"


def afficher_menu() -> str:
    """
    Afficher le menu des fonctions possibles.
    :return: Le choix de l'utilisateur.
    """
    return input(textwrap.dedent("""
    1 - Type de valeur
    2 - Nombre premier le plus proche
    3 - Transformer mot
    
    Quel est votre choix ? """))


def main():
    str_choix_fct = afficher_menu()

    # type_de_valeur
    if str_choix_fct == "1":
        valeur_a_tester = input("Veuillez entrer une valeur : ")
        print(f"{Fore.CYAN}{valeur_a_tester}{Fore.RESET} est de type {Fore.CYAN}{type_de_valeur(valeur_a_tester)}")

    # Nombre premier le plus proche
    elif str_choix_fct == "2":
        nombre = int(input("Veuillez entrer un nombre : "))
        print(f"Le nombre premier le plus proche de {Fore.CYAN}{nombre}{Fore.RESET} est {Fore.CYAN}{nombre_premier_plus_proche(nombre)}")

    # Transformer mot
    elif str_choix_fct == "3":
        mot = input("Veuillez entrer un mot : ")
        print(f"Le mot {Fore.CYAN}{mot}{Fore.RESET} transformé est {Fore.CYAN}{transformer_mot(mot)}")

    else:
        print("Votre sélection n'est pas valide. Au revoir.")


if __name__ == "__main__":
    main()
