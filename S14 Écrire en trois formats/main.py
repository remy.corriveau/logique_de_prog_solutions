import ecrire
import textwrap


def afficher_menu() -> str:
    """
    Afficher le menu des fonctions possibles.
    :return: Le choix de l'utilisateur.
    """
    return input(textwrap.dedent("""
    1 - Un beau message
    2 - Calculer moyenne
    3 - Générateur de personnages
    4 - Combat personnages
    5 - Ventes de jeux PS4
    
    Quel est votre choix ? """))


if __name__ == '__main__':
    
    choix = afficher_menu()

    if choix == "1":
        # Message secret
        message = input("Quel est le message : ")
        fichier = input("Quel est le nom du fichier pour la sauvegarde : ")
        ecrire.un_beau_message(message, fichier)
        print(f"Message sauvegardé dans le fichier : {fichier}")

    elif choix == "2":
        # Calculer moyenne
        print(ecrire.calculer_moyenne("notes.txt"))

    elif choix == "3":
        # Générateur de personnage
        perso = input("Quel est le nom du personage à créer ? ")
        ecrire.generateur_personnage(perso)

    elif choix == "4":
        # combat
        perso1 = input("Quel est le nom du premier combattant ? ")
        perso2 = input("Quel est le nom du deuxième combattant ? ")
        ecrire.combat_personnages(perso1, perso2)

    elif choix == "5":
        # PS4
        annee = input("Pour quelle année voulez-vous trouver le meilleur vendeur ? ")
        ecrire.ventes_jeux_ps4(annee)
