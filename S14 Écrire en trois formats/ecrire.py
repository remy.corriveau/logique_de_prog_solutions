import random
import json
import csv
from textwrap import dedent
import chiffrement_sym
from colorama import Fore, init
init(autoreset=True)


def un_beau_message(message: str, ch_fichier: str) -> None:
    """
    Écrit un message texte dans le fichier spécifié.

    :param message: Message à écrire.
    :param ch_fichier: Chemin et nom du fichier.
    """
    # Pour le bonus de chiffrement, ça va être indéchiffrable si on ne conserve pas la clé.
    cle_chiffrement = chiffrement_sym.generer_cle()
    message_chiffre = chiffrement_sym.chiffrer_message(message.encode(), cle_chiffrement).decode()

    with open(ch_fichier, "w") as f:
        f.write(message_chiffre)


def calculer_moyenne(ch_fichier: str) -> float:
    """
    Lis un fichier contenant des nombres, calcule et retourne la moyenne.

    :param ch_fichier: Chemin et nom du fichier.
    :return: Moyenne des nombres.
    """
    with open(ch_fichier, "r") as f:
        nombres = []
        for nombre in f.readlines():
            nombres.append(float(nombre))

    return sum(nombres) / len(nombres)

def generateur_personnage(nom_perso: str) -> None:
    """
    Génère un personnage avec des stats de vie et de puissance aléatoire.
    Sauvegarge le personnage dans un fichier à son nom.

    :param nom_perso: Nom.
    """
    with open(f"{nom_perso}.json", "w") as f:
        json.dump({
            "nom": nom_perso,
            "vie": random.randint(10, 20),
            "puissance": random.randint(1, 5)
        }, f, indent=4)


def combat_personnages(nom_perso1: str, nom_perso2: str) -> None:
    """
    Lis les fichiers associer aux combatant en paramètre. Affiche le résultat du combat.

    :param nom_perso1: Nom du combattant 1.
    :param nom_perso2: Nom du combattant 2.
    """
    # Extraire les combattants des fichiers
    with open(f"{nom_perso1}.json", "r") as f:
        combattant1 = json.load(f)
    with open(f"{nom_perso2}.json", "r") as f:
        combattant2 = json.load(f)

    # Combat
    while combattant1["vie"] > 0 and combattant2["vie"] > 0:
        # Combattant 1 attaque
        combattant2["vie"] -= combattant1["puissance"]
        # Combattant 2 attaque
        combattant1["vie"] -= combattant2["puissance"]

    # Afficher le résultat
    if combattant1["vie"] <= 0 and combattant2["vie"] <= 0:
        print(f"{Fore.RED}Les deux combattants sont morts !")
    elif combattant1["vie"] > 0:
        print(f"{Fore.GREEN}{combattant1['nom']} a gagné !")
    else:
        print(f"{Fore.GREEN}{combattant2['nom']} a gagné !")


def ventes_jeux_ps4(annee: str) -> None:
    """
    Trouve et affiche le jeu le plus vendu selon l'année demandée.
    Utilise le fichier : PS4_GamesSales.csv

    :param annee: 2014-2018, en string
    """
    # format du fichier PS4 [{'Game': 'Radial G Racing Revolved', 'Year': '2017', 'Genre': 'Racing', 'Publisher':
    # 'Tammeka Games','North America': '0', 'Europe': '0', 'Japan': '0', 'Rest of World': '0', 'Global': '0'}]
    with open("PS4_GamesSales.csv", "r") as f:
        jeux = list(csv.DictReader(f))

    # Trouver le jeu le plus vendu
    jeu_plus_vendu = None
    for jeu in jeux:
        if jeu["Year"] == annee:
            if jeu_plus_vendu is None:
                jeu_plus_vendu = jeu
            elif float(jeu["Global"]) > float(jeu_plus_vendu["Global"]):
                jeu_plus_vendu = jeu

    # Afficher le résultat
    print(dedent(f"""
    Nom    : {Fore.GREEN}{jeu_plus_vendu["Game"]}{Fore.RESET}
    Genre  : {Fore.GREEN}{jeu_plus_vendu["Genre"]}{Fore.RESET}
    Ventes : {Fore.GREEN}{jeu_plus_vendu["Global"]} millions{Fore.RESET}
    """))
