import sys
from colorama import Fore, init
from cryptography.fernet import Fernet
from textwrap import dedent
init(autoreset=True)


def afficher_menu() -> str:
    """
    Afficher le menu de l'application.
    :return: Le choix de l'utilisateur.
    """
    print(dedent("""
        1. Afficher la clé secrète
        2. Entrer une clé secrète manuellement
        3. Chiffrer un message
        4. Déchiffrer un message
        5. Quitter
    """))
    return input("Votre choix : ")


def generer_cle():
    """
    Generates a random key for symmetric encryption.
    Fernet utilise l'algorithme AES.

    Returns:
    - bytes: The generated key.
    """
    return Fernet.generate_key()


def chiffrer_message(message, cle):
    """
    Encrypts a message using symmetric encryption.

    Args:
    - message (bytes): The message to be encrypted.
    - key (bytes): The symmetric encryption key.

    Returns:
    - bytes: The encrypted message.
    """
    algo_plus_cle = Fernet(cle)
    encrypted_message = algo_plus_cle.encrypt(message)
    return encrypted_message


def dechiffrer_message(message_chiffre, cle):
    """
    Decrypts an encrypted message using symmetric encryption.

    Args:
    - encrypted_message (bytes): The encrypted message to be decrypted.
    - key (bytes): The symmetric encryption key.

    Returns:
    - bytes: The decrypted message.
    """
    cipher = Fernet(cle)
    message_dechiffre = cipher.decrypt(message_chiffre)
    return message_dechiffre.decode()


def main():
    # On génère toujours une nouvelle clé secrète
    cle_secrete = generer_cle()

    print(f"{Fore.BLUE}Bienvenue dans le 'meilleur chiffrement' !")
    while True:
        action = afficher_menu()

        # 1. Afficher la clé secrète
        if action == "1":
            # Afficher la clé secrète
            print("La clé secrète est : ", cle_secrete.decode())

        # 2. Entrer une clé secrète manuellement
        elif action == "2":
            # Demander à l'utilisateur d'entrer une clé secrète
            cle_secrete = input("Entrez une clé secrète : ").encode()
            print(f"{Fore.GREEN}La clé secrète a été mise à jour !")

        # 3. Chiffrer un message
        elif action == "3":
            # Demander à l'utilisateur d'entrer un message
            message = input("Entrez un message : ")
            # Chiffrer le message
            message_chiffre = chiffrer_message(message.encode(), cle_secrete)
            # Afficher le message chiffré
            print(f"Le message chiffré est : {Fore.RED}{message_chiffre.decode()}")

        # 4. Déchiffrer un message
        elif action == "4":
            # Demander à l'utilisateur d'entrer un message chiffré
            message_chiffre = input("Entrez un message chiffré : ")
            # Déchiffrer le message
            message = dechiffrer_message(message_chiffre.encode(), cle_secrete)
            # Afficher le message déchiffré
            print(f"Le message déchiffré est : {Fore.GREEN}{message}")

        # 5. Quitter
        elif action == "5":
            print(f"{Fore.BLUE}Merci d'avoir utilisé le meilleur chiffrement !")
            sys.exit()

        # Si l'utilisateur entre un choix invalide
        else:
            print(f"{Fore.RED}Veuillez entrer un choix valide !")


if __name__ == "__main__":
    main()
