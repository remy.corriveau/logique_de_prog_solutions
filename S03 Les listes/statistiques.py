# Déclaration de variables
ls_nombres = [47, 90, 36, 85, 59, 43, 75, 89, 24, 13, 35, 65, 73, 41, 81, 27, 32, 64, 95, 54]

# Calculs
taille = len(ls_nombres)
valeur_min = min(ls_nombres)
valeur_max = max(ls_nombres)
somme = sum(ls_nombres)
liste_croissante = sorted(ls_nombres)
liste_decroissante = sorted(ls_nombres, reverse=True)

# Affichage
print(f"""
Taille:            {taille}
Valeur minimale:   {valeur_min}
Valeur maximale:   {valeur_max}
Somme des nombres: {somme}
Liste ordonnée:    {liste_croissante}
Liste à l'envers:  {liste_decroissante}
""")
