# Lire la séquence de mots
str_mots = input("Veuillez entrer une série d'item de votre choix : ")

# Séparer la string à tous les " "
ls_mots = str_mots.split(" ")

# Afficher le contenu de la liste
print(ls_mots)

# Afficher chaque mot sur une nouvelle ligne (optionnel)
for mot in ls_mots:
    print(mot)
